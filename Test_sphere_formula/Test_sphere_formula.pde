void setup() {
  size(800, 800, P3D);
}

void draw() {
  background(0);
  stroke(255);
  strokeWeight(2);
  translate(width/2, height/2);
  rotateY(millis() * 0.0005);
  for (float u=0; u<1; u+=0.02) {
    for (float v=0; v<1; v+=0.1) {

      PVector vec = new PVector(
        cos(u * TWO_PI) * sin(v * PI), 
        cos(v * PI), 
        sin(u * TWO_PI) * sin(v * PI));
      vec.mult(200);
      point(vec.x, vec.y, vec.z);
    }
  }
}
