# Install fftw3 

Because the one in ofxAudioAnalyzer doesn't compile

```
cd /tmp/
wget https://www.fftw.org/fftw-3.3.10.tar.gz
untar fftw-3.3.10.tar.gz
cd fftw-3.3.10
./configure --prefix /tmp/FFT/ --enable-float
make
make install
cp /tmp/FFT/lib/libfftw3f.a ~/src/openFrameworks/addons/ofxAudioAnalyzer/libs/fftw3f/lib/linux64
```

# Dependencies

- https://github.com/hamoid/ofxCereal
- https://github.com/hamoid/ofxAudioAnalyzer
- https://github.com/andreasmuller/ofxAutoReloadedShader

# Update

- Open GitHub Desktop
- Make sure "Current Repository" is "blobof"
- Click "Fetch origin"
- Click "Pull origin"
- Close GitHub Desktop

## Only if the addons changed

- Run /Users/blob/GitHub/openFrameworks/projectGenerator
- Import the `GitHub/blobof/BlobOF` project
- Adjust addons. To find out which addons I'm using, open BlobOF/BlobOF.qbs in TextEdit
  and study the section `of.addons: [ ... ]`. The same addons should be used in XCode plus 
  `ofxAudioDecoder`, which is needed in Mac but not on Linux
- Scroll down and click the orange Update button.
- Open XCode
- Delete ofApp.cpp and ofApp.h
- Click BlobOF on the left pane
- Click Build Setings
- In Architectures > Excluded Architectures type "arm64" if missing
- In Apple Clang - Language - C++ > C++ Language Dialect select C++14
  to fix compilation error dealing with `time` in ofxAutoReloadedShader

# blobConfig.json

There are variables that can be adjusted in `bin/data/blobConfig.json`.
The main one is `multiWindow` and maybe `win` and `ui` size and position.
Other variables could make the program not work properly.

(test change)

## NOTES OCT 2022

- [x] implement midi listening
- [x] midi: sound pause
- [x] midi: sound play
- [x] midi: sound volume
- [x] midi: next song
- [x] midi: prev song
- [x] load song 0 automatically but leave it paused
- [x] make it possible to fade screen to black
- [?] see if transparency is possible. Haven't figured out how to do it.
- [x] add debug mode MIDI button
- leaving marks with loud sound (sometimes) on the shape,
- marks fade out with time partially

## Complaining

- QtCreator doesn't show return types for offered autocompletion methods.
- QtCreator only autocompletes from the beginning. Doesn't search in the middle of a method's name.
- C++: switch case needs break
- C++: switch has no range
