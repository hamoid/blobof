#version 330

uniform mat4 modelViewMatrix; // optional
uniform mat4 projectionMatrix; // optional

uniform float mouseInteraction;
uniform vec2 mousePos;

uniform vec4 volumeMicrophone;
uniform vec4 microphoneScaleSpectrums;
uniform float microphoneScaleStrength;

uniform vec4 microphoneBigNoiseScaleSpectrums;
uniform vec2 microphoneBigNoiseScaleRange;
uniform vec4 microphoneBigNoiseOffsetSpectrums;
uniform vec2 microphoneBigNoiseOffsetRange;

uniform vec4 microphoneSmallNoiseScaleSpectrums;
uniform vec2 microphoneSmallNoiseScaleRange;
uniform vec4 microphoneSmallNoiseOffsetSpectrums;
uniform vec2 microphoneSmallNoiseOffsetRange;

uniform vec4 volumeSong;
uniform vec4 songScaleSpectrums;
uniform float songScaleStrength;

uniform vec4 songBigNoiseScaleSpectrums;
uniform vec2 songBigNoiseScaleRange;
uniform vec4 songBigNoiseOffsetSpectrums;
uniform vec2 songBigNoiseOffsetRange;

uniform vec4 songSmallNoiseScaleSpectrums;
uniform vec2 songSmallNoiseScaleRange;
uniform vec4 songSmallNoiseOffsetSpectrums;
uniform vec2 songSmallNoiseOffsetRange;

uniform vec3 cursorWorldSmoothed;
uniform float yRotation;
uniform float positionZ;
uniform vec3 shapeNoiseData;
uniform vec3 bigNoiseData;
uniform vec3 smallNoiseData;
uniform vec4 deflateNoiseGradientData;
uniform sampler2D smoothedNormalsMap;
uniform sampler2D wrinklesMap;

// ---

in vec2 vUV;
in vec3 vCenterPosition;
in float vAttractionValue;
in float vDeflateAmount;
in float vAudioReactiveness;

out vec4 outputColor;

#pragma include "shaders/includes/noiseShared.glsl"
#pragma include "shaders/includes/noise3D.glsl"
#pragma include "shaders/includes/noise3Dgrad.glsl"
#pragma include "shaders/includes/noise4D.glsl"

const float phiLength   = 6.28318530718;
const float thetaLength = 3.14159265359;

mat3 rotateY(float rad) {
    float c = cos(rad);
    float s = sin(rad);
    return mat3(
            c, 0.0, -s,
            0.0, 1.0, 0.0,
            s, 0.0, c
        );
}

float easeOutCubic(float t) {
    return (t - 1.0) * t * t + 1.0;
}

float inverseLerpClamped(float value, float a, float b) {
    // NOTE: had wrong order of arguments
    return clamp((value - a) / (b - a), 0.0, 1.0);
}

float when_gt(float x, float y) {
    return max(sign(x - y), 0.0);
}

void main() {
    vec3 vNormal = vec3(
            cos(vUV.x * phiLength) * sin(vUV.y * thetaLength),
            cos(vUV.y * thetaLength),
            sin(vUV.x * phiLength) * sin(vUV.y * thetaLength)
        );

    vec3 smoothedSurfaceNormal = texture(smoothedNormalsMap, vUV).xyz;
    float wrinkleHeight = texture(wrinklesMap, vUV).r;

    vec3 gradient3D;
    vec3 noisePos = vNormal * shapeNoiseData.x;
    noisePos.z += shapeNoiseData.y;

    // Update both `noise` AND `gradient3D`
    float noise = snoise(noisePos, gradient3D);

    vec3 position = mix(vNormal, gradient3D, shapeNoiseData.z);

    position *= 0.5 + wrinkleHeight;

    vec4 viewPos = modelViewMatrix * vec4(position + vCenterPosition, 1.0);
    vec4 screenPosition = projectionMatrix * viewPos;
    screenPosition.xy /= screenPosition.w;

    vec2 toMouseCursor = screenPosition.xy - mousePos;
    float toMouseCursorDistance = length(toMouseCursor);
    toMouseCursorDistance += noise * 0.05;

    float mouseCursorEffect = smoothstep(0.2, 0.0, toMouseCursorDistance);
    mouseCursorEffect *= 0.13 * mouseInteraction;

    position -= vNormal * mouseCursorEffect;

    // apply sound
    position *= 1.0 + noise * microphoneScaleStrength * 0.25 *
        dot(microphoneScaleSpectrums, volumeMicrophone) * vAudioReactiveness;

    // apply song
    position *= 1.0 + songScaleStrength * 0.25 *
        dot(songScaleSpectrums, volumeSong) * vAudioReactiveness;

    // shrink when mouse is interacting
    position *= 1.0 - (mouseInteraction * (0.05 + noise * 0.05));

    // add attraction
    vec3 toCursor = cursorWorldSmoothed - vCenterPosition;
    float toCursorDistance = length(toCursor);
    vec3 toCursorNorm = normalize(toCursor);
    float toCursorDot = dot(
            normalize(vNormal),
            toCursorNorm
        );

    float attractionAmount = 0.0;
    attractionAmount += max(toCursorDot, 0.0) * 0.4;
    attractionAmount += easeOutCubic(
            inverseLerpClamped(toCursorDot, -0.2, 0.8)) * 0.6;
    attractionAmount *= 1.0 + noise * 0.5;
    attractionAmount *= 0.6;
    attractionAmount *= vAttractionValue;

    position += mix(
            toCursorNorm,
            toCursorNorm * min(toCursorDistance, 0.5),
            when_gt(attractionAmount, 0.0)
        ) * (attractionAmount * mouseInteraction);

    float slimmerOnAttraction = smoothstep(0.8, 0.0, abs(toCursorDot));
    position -= vNormal * (slimmerOnAttraction * clamp(-1.0, 0.1,
                attractionAmount) * 0.5 * mouseInteraction);

    float noiseDefalteAmount = 0.6 + 0.4 * vDeflateAmount;

    float microphoneBigNoiseScale = mix(
            microphoneBigNoiseScaleRange.x,
            microphoneBigNoiseScaleRange.y,
            dot(microphoneBigNoiseScaleSpectrums, volumeMicrophone) * noiseDefalteAmount
        );
    float microphoneBigNoiseOffset = vAudioReactiveness * mix(
            microphoneBigNoiseOffsetRange.x,
            microphoneBigNoiseOffsetRange.y,
            dot(microphoneBigNoiseOffsetSpectrums, volumeMicrophone) * noiseDefalteAmount
        );
    float songBigNoiseScale = mix(
            songBigNoiseScaleRange.x,
            songBigNoiseScaleRange.y,
            dot(songBigNoiseScaleSpectrums, volumeSong) * noiseDefalteAmount
        );
    float songBigNoiseOffset = vAudioReactiveness * mix(
            songBigNoiseOffsetRange.x,
            songBigNoiseOffsetRange.y,
            dot(songBigNoiseOffsetSpectrums, volumeSong) * noiseDefalteAmount
        );
    position += vNormal * snoise(
            vec4(vNormal * bigNoiseData.x * songBigNoiseScale *
                microphoneBigNoiseScale, bigNoiseData.y)
        ) * bigNoiseData.z * songBigNoiseOffset * microphoneBigNoiseOffset;


    float microphoneSmallNoiseScale = mix(
            microphoneSmallNoiseScaleRange.x,
            microphoneSmallNoiseScaleRange.y,
            dot(microphoneSmallNoiseScaleSpectrums, volumeMicrophone) * noiseDefalteAmount
        );
    float microphoneSmallNoiseOffset = mix(
            microphoneSmallNoiseOffsetRange.x,
            microphoneSmallNoiseOffsetRange.y,
            dot(microphoneSmallNoiseOffsetSpectrums, volumeMicrophone) * noiseDefalteAmount
        );
    float songSmallNoiseScale = mix(
            songSmallNoiseScaleRange.x,
            songSmallNoiseScaleRange.y,
            dot(songSmallNoiseScaleSpectrums, volumeSong) * noiseDefalteAmount
        );
    float songSmallNoiseOffset = mix(
            songSmallNoiseOffsetRange.x,
            songSmallNoiseOffsetRange.y,
            dot(songSmallNoiseOffsetSpectrums, volumeSong) * noiseDefalteAmount
        );
    position += vNormal * snoise(
            vec4(vNormal * smallNoiseData.x * songSmallNoiseScale *
                microphoneSmallNoiseScale, smallNoiseData.y)
        ) * smallNoiseData.z * songSmallNoiseOffset * microphoneSmallNoiseOffset;

    float deflateNoise = snoise(
            position * deflateNoiseGradientData.x +
            gradient3D * deflateNoiseGradientData.z,
            gradient3D);

    vec3 deflatePosition = position;
    deflatePosition += normalize(gradient3D) * deflateNoiseGradientData.y;
    deflatePosition *= mix(1.0, deflateNoise, deflateNoiseGradientData.w);
    deflatePosition *= 0.95;

    position = mix(deflatePosition, position, vDeflateAmount);

    position *= smoothstep(-1.0, -0.1, positionZ);
    position = rotateY(yRotation) * position;
    position += vCenterPosition;

    outputColor = vec4(position, 1.0);
}
