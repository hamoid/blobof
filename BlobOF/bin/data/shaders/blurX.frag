#version 330

uniform sampler2D tex0;

in vec2 texCoordVarying;
out vec4 outputColor;

// Gaussian weights from http://dev.theomader.com/gaussian-kernel-calculator/

void main() {

  vec4 color = vec4(0.0);

  float amt = 1.0;
	
  color += 0.000229 * texture(tex0, texCoordVarying + vec2(amt * -4.0, 0.0));
  color += 0.005977 * texture(tex0, texCoordVarying + vec2(amt * -3.0, 0.0));
  color += 0.060598 * texture(tex0, texCoordVarying + vec2(amt * -2.0, 0.0));
  color += 0.241732 * texture(tex0, texCoordVarying + vec2(amt * -1.0, 0.0));

  color += 0.382928 * texture(tex0, texCoordVarying);

  color += 0.241732 * texture(tex0, texCoordVarying + vec2(amt * 1.0, 0.0));
  color += 0.060598 * texture(tex0, texCoordVarying + vec2(amt * 2.0, 0.0));
  color += 0.005977 * texture(tex0, texCoordVarying + vec2(amt * 3.0, 0.0));
  color += 0.000229 * texture(tex0, texCoordVarying + vec2(amt * 4.0, 0.0));

  outputColor = vec4(color.rgb, 1.0);
}

