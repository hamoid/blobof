#version 330

uniform float normalProcessingSmoother;
uniform mat4 modelViewProjectionMatrix;
uniform vec3 offset;

in vec4 position;
in vec2 texcoord;

out vec2 vUV;

out vec2 vUp;
out vec2 vLeft;
out vec2 vDown;
out vec2 vRight;

out vec2 vTL;
out vec2 vTR;
out vec2 vDL;
out vec2 vDR;

void main() {
    vUV = texcoord;

    vUp = vUV + offset.zy * normalProcessingSmoother;
    vLeft = vUV - offset.xz * normalProcessingSmoother;
    vDown = vUV - offset.zy * normalProcessingSmoother;
    vRight = vUV + offset.xz * normalProcessingSmoother;

    vTL = vUV + vec2(-offset.x, offset.y) * normalProcessingSmoother;
    vTR = vUV + offset.xy * normalProcessingSmoother;
    vDL = vUV - offset.xy * normalProcessingSmoother;
    vDR = vUV + vec2(offset.x, -offset.y) * normalProcessingSmoother;

    gl_Position = modelViewProjectionMatrix * position;
}
