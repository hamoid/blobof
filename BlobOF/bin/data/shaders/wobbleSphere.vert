#version 330

uniform mat4 modelViewProjectionMatrix;
uniform mat4 modelViewMatrix;

uniform sampler2D positionsMap;
uniform sampler2D smoothedNormalsMap;

in vec2 texcoord;

out vec2 vUV;
out vec3 vNormal;
out float vRimDot;

#pragma include "shaders/includes/noiseShared.glsl"
#pragma include "shaders/includes/noise3Dgrad.glsl"

void main()	{
    vUV = texcoord;

    // normalMatrix is not set by default in openFrameworks
    // https://forum.openframeworks.cc/t/how-to-tweak-the-default-vertex-and-fragment-shaders/32588/11
    // https://forum.openframeworks.cc/t/light-using-glsl-150/14332/8
    mat3 normalMatrix = transpose(inverse(mat3(modelViewMatrix)));
    vNormal = normalMatrix * texture(smoothedNormalsMap, vUV).xyz;

    vNormal = normalize(vNormal);

    vRimDot = 1.0 - dot(vNormal, vec3(0.0, 0.0, -1.0));
    vRimDot = max(vRimDot, 0.00001);

    gl_Position = modelViewProjectionMatrix * texture(positionsMap, vUV);
}
