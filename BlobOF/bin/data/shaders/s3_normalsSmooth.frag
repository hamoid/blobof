#version 330

uniform sampler2D normalsMap;

in vec2 vUV;

in vec2 vUp;
in vec2 vLeft;
in vec2 vDown;
in vec2 vRight;

in vec2 vTL;
in vec2 vTR;
in vec2 vDL;
in vec2 vDR;

out vec4 outputColor;

void main() {
    vec3 averagedNormal;

    averagedNormal = 0.195346 * texture(normalsMap, vUV).xyz; // center

    averagedNormal += 0.123317 * texture(normalsMap, vUp).xyz; // top
    averagedNormal += 0.123317 * texture(normalsMap, vLeft).xyz; // left
    averagedNormal += 0.123317 * texture(normalsMap, vDown).xyz; // down
    averagedNormal += 0.123317 * texture(normalsMap, vRight).xyz; // right

    averagedNormal += 0.077847 * texture(normalsMap, vTL).xyz; // tl
    averagedNormal += 0.077847 * texture(normalsMap, vTR).xyz; // tr
    averagedNormal += 0.077847 * texture(normalsMap, vDL).xyz; // dl
    averagedNormal += 0.077847 * texture(normalsMap, vDR).xyz; // dr

    outputColor = vec4(averagedNormal, 1.0);
}
