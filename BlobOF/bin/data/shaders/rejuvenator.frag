#version 330

/*
  This shader fades colors towards 0.5.

  Positive values are raised, negative values are below the surface.
  I didn't know how to use negative colors in openFrameworks
  so I used this approach instead.

  The fbo where this shader is applied is an elevation map.
  On loud sounds elevation is painted on that map.
  As time passes, those elevations are "rejuvenated",
  so the damage is reverted.
*/

uniform sampler2D tex0;

/*
  How much can the map be rejuvenated.
  1.0 = back to new.
  0.5 = rejuvenate only half way to new.
  0.0 = not at all.
*/
uniform float amount;

/*
  How fast the rejuvenation happens.
  0.00 =   0% rejuvenation per frame.
  0.01 =   1% rejuvenation per frame.
  1.00 = 100% rejuvenation per frame.
*/
uniform float speed;

in vec2 texCoordVarying;
out vec4 outputColor;

void main() {
  // Value between -1.0 and 1.0
  float elevation = texture(tex0, texCoordVarying).r - 0.5;

  float sgn = sign(elevation);
  float mag = abs(elevation);
  float eleMax = 0.5 - amount * 0.5;

  if(mag > eleMax) {
      // shrink towards 0
      elevation = max(eleMax, mag - speed) * sgn;
  }

  outputColor = vec4(vec3(elevation + 0.5), 1.0);
}

