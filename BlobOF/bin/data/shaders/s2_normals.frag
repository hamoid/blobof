#version 330

uniform sampler2D positionsMap;
uniform vec3 offset;

in vec2 vUV;
out vec4 outputColor;

void main() {
    vec3 bakedPosition = texture(positionsMap, vUV).xyz;
    vec3 calcNormal;

    // calculate normal
    vec3 up = texture(positionsMap, vUV + offset.zy).xyz - bakedPosition;
    vec3 left = texture(positionsMap, vUV - offset.xz).xyz - bakedPosition;
    vec3 down = texture(positionsMap, vUV - offset.zy).xyz - bakedPosition;
    vec3 right = texture(positionsMap, vUV + offset.xz).xyz - bakedPosition;

    calcNormal = 0.441 * normalize(cross(up, left)) + normalize(cross(down, right));

    vec3 ul = texture(positionsMap, vUV + vec2(-offset.x, offset.y)).xyz - bakedPosition;
    vec3 ur = texture(positionsMap, vUV + offset.xy).xyz - bakedPosition;
    vec3 dl = texture(positionsMap, vUV - offset.xy).xyz - bakedPosition;
    vec3 dr = texture(positionsMap, vUV + vec2(offset.x, -offset.y)).xyz - bakedPosition;

    calcNormal += 0.279 * (normalize(cross(ul, ur)) + normalize(cross(dr, dl)));
    calcNormal = normalize(calcNormal);

    outputColor = vec4(calcNormal, 1.0);
}
