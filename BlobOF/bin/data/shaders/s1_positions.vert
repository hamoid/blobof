#version 330

uniform float aspectRatio;
uniform vec3 attractPositionWorld;
uniform float mouseInteraction;
uniform vec3 positionNoiseData;
uniform vec3 avoidPositionWorld;
uniform vec3 behaviourNoiseData;
uniform float positionZDistance;
uniform float positionZ;
uniform vec2 deflateNoiseData;
uniform vec3 audioReactivenessNoise;

uniform mat4 modelViewProjectionMatrix;

in vec4 position;
in vec2 texcoord;

out vec2 vUV;
out vec3 vCenterPosition;
out float vAttractionValue;
out float vDeflateAmount;
out float vAudioReactiveness;

#pragma include "shaders/includes/noiseShared.glsl"
#pragma include "shaders/includes/noise2D.glsl"
#pragma include "shaders/includes/noise3D.glsl"

void main() {
    vUV = texcoord;

    vAudioReactiveness =
        audioReactivenessNoise.y + audioReactivenessNoise.z *
        snoise(vec2(10.0, audioReactivenessNoise.x));


    vAttractionValue =
        behaviourNoiseData.x + behaviourNoiseData.z *
        snoise(vec2(0.0, behaviourNoiseData.y));

    // NOTE: this had the wrong order of arguments.
    vAttractionValue = clamp(vAttractionValue, -0.9, 0.9);

    vCenterPosition = vec3(0.0);

    float avoidAmount = max(-vAttractionValue, 0.0) * mouseInteraction;
    vCenterPosition = mix(vCenterPosition, avoidPositionWorld, avoidAmount);

    float attractAmount = max(vAttractionValue, 0.0) * mouseInteraction;
    vCenterPosition = mix(vCenterPosition, attractPositionWorld, attractAmount);

    vec3 noise3d = vec3(
            snoise(vec2(0.0, positionNoiseData.x)),
            snoise(vec2(-20.0, positionNoiseData.x)),
            snoise(vec2(30.0, positionNoiseData.x))
    );
    vCenterPosition += positionNoiseData.yyz * noise3d;
    vCenterPosition.z += positionZ * positionZDistance;

    vDeflateAmount = snoise(vec2(1.0, deflateNoiseData.x));
    vDeflateAmount = smoothstep(0.0, deflateNoiseData.y, abs(vDeflateAmount));

    gl_Position = modelViewProjectionMatrix * position;
}
