#version 330

uniform sampler2D positionsMap;

uniform float noiseNormalScale0;
uniform float noiseNormalTime0;
uniform float noiseNormalStrength0;

uniform float noiseNormalScale1;
uniform float noiseNormalTime1;
uniform float noiseNormalStrength1;

in vec2 vUV;

out vec4 outputColor;

#pragma include "shaders/includes/noiseShared.glsl"
#pragma include "shaders/includes/noise3Dgrad.glsl"

void main() {
  float TWO_PI = 6.28318530718;
  float PI = 3.14159265359;

  vec3 vNormal;
  vNormal.x = cos(vUV.x * TWO_PI) * sin(vUV.y * PI);
  vNormal.y = cos(vUV.y * PI);
  vNormal.z = sin(vUV.x * TWO_PI) * sin(vUV.y * PI);

  vec3 normal0;
  vec3 noisePosition = vNormal * noiseNormalScale0;
  noisePosition.z += noiseNormalTime0;
  snoise(noisePosition, normal0);

  vec3 normal1;
  noisePosition = vNormal * noiseNormalScale1;
  noisePosition.z += noiseNormalTime1;
  snoise(noisePosition, normal1);


  normal0 =
    normal0 * noiseNormalStrength0 +
    normal1 * noiseNormalStrength1;
  normal0 = normalize(normal0);
  normal0 = 0.5 + 0.5 * normal0;

  outputColor = vec4(normal0, 1.0);
}
