#version 330

uniform float time;
uniform sampler2D noiseNormalMap;
uniform float noiseNormalAmount;
uniform float normalNoiseDistance;

uniform vec3 backgroundColor;
uniform vec3 baseColor;

uniform vec3 rimColor;
uniform float rimPower;
uniform float rimScale;

uniform vec3 spotColor;
uniform vec3 spotVector;
uniform float spotPower;
uniform float spotScale;

uniform float aaPower;
uniform float aaScale;
uniform float opacity;

in vec2 vUV;
in vec3 vNormal;
in float vRimDot;

out vec4 outputColor;

#pragma include "shaders/includes/perturbNormal.glsl"
#pragma include "shaders/includes/random.glsl"

void main() {
    vec2 noiseNormalUV = vUV + normalNoiseDistance * (random(vec2(vUV.x, vUV.y + time)) - 0.5);

    //mediump
    vec3 noiseMapData = texture(noiseNormalMap, noiseNormalUV).xyz;
    vec3 noiseNormal = -0.95 + 1.9 * noiseMapData;
    noiseNormal *= noiseNormalAmount;
    vec3 perturbedNormal = normalize(vNormal + noiseNormal);

    vec3 color = baseColor;

    float rimDot = 1.0 - dot(perturbedNormal, vec3(0.0, 0.0, -1.0));
    rimDot = max(rimDot, 0.00001);

    float rimValue = rimScale * pow(abs(rimDot), rimPower);
    color += rimValue * rimColor;

    // add spotlight
    float spotValue = spotScale * pow(max(0.0, dot(normalize(spotVector),
                    perturbedNormal)), spotPower);
    color += spotColor * spotValue;

    // fake anti aliasing
    color = mix(color, backgroundColor, aaScale * pow(abs(vRimDot), aaPower));

    outputColor = vec4(color, opacity);
}
