#pragma once

#include "config.h"
#include "ofSoundPlayerExtended.h"
#include "ofxAudioAnalyzer.h"

class AudioFile {
    private:
        shared_ptr<BlobConfig> cfg;
        ofSoundPlayerExtended player;
        ofSoundStreamSettings settings;
        ofxAudioAnalyzer analyzer;
        ofSoundBuffer buffer;

        bool isAnalyzerSetup { false };

        float average(vector<float> & data, size_t a, size_t b);
        float volumeCurr { 0.1f };
        float volumeNext { 0.1f };

    public:
        void setup(shared_ptr<BlobConfig> cfg);
        void update(float smooth);
        void toggle();
        void play();
        void pause();
        void load(ofFile file);
        void exit();
        void setVolume(float volNorm);
        bool anyBandAbove(float threshold);

        float getPlayerPos();
        float rms;
        float bandMaxValue;

        glm::vec4 bands { 0.0 };

        vector<float> spectrum;
};

