#include "wobblehandler.h"
#include "glm/common.hpp"
#include "config.h"

//--------------------------------------------------------------
void WobbleHandler::makeBlobGeometry() {
    int lastVertexIndex = -1;

    // add uvs and indices
    for (int y = 1, l = cfg->res.y - 1; y < l; y++) {
        float v = y / (l + 1.0f);

        for (int x = 0, l2 = cfg->res.x; x <= l2; x++) {
            float u = x / static_cast<float>(l2);

            // This was attribute "uv"
            mesh.addTexCoord(glm::vec2(u, v));
            mesh.addVertex(glm::vec3(0.0));

            lastVertexIndex++;

            // set quads
            if (x > 0 && y > 1) {
                // triangle 1
                mesh.addIndex(lastVertexIndex - cfg->res.x - 1);
                mesh.addIndex(lastVertexIndex);
                mesh.addIndex(lastVertexIndex - 1);
                // triangle 2
                mesh.addIndex(lastVertexIndex - cfg->res.x - 1);
                mesh.addIndex(lastVertexIndex - 1);
                mesh.addIndex(lastVertexIndex - cfg->res.x - 2);
            }
        }
    }

    // set top triangles
    for (int x = 2, l2 = cfg->res.x; x <= l2; x++) {
        mesh.addIndex(x - 1);
        mesh.addIndex(0);
        mesh.addIndex(x);
    }

    // set bottom triangles
    for (int x = 1, l2 = cfg->res.x; x <= l2; x++) {
        mesh.addIndex(lastVertexIndex);
        mesh.addIndex(lastVertexIndex - x - 1);
        mesh.addIndex(lastVertexIndex - x);
    }
}

//--------------------------------------------------------------
void WobbleHandler::configureArea(ofPlanePrimitive & a,
    const glm::vec2 & size) {
    a.set(size.x, size.y);
    a.setPosition(size.x / 2.f, size.y / 2.f, 0.f);
    a.mapTexCoords(0, 0, 1, 1);
}

//--------------------------------------------------------------
void WobbleHandler::setup(
    shared_ptr<Uniforms> uniforms,
    shared_ptr<Gui> gui,
    shared_ptr<ofCamera> cam,
    shared_ptr<BlobConfig> cfg) {

    this->uniforms = uniforms;
    this->gui = gui;
    this->cam = cam;
    this->cfg = cfg;

    configureArea(area, cfg->res);
    configureArea(areaNoise, cfg->resNoise);

    shdr1Positions.load("shaders/s1_positions");
    shdr2Normals.load("shaders/s2_normals");
    shdr3NormalsSmooth.load("shaders/s3_normalsSmooth");
    shdr4NormalsNoise.load("shaders/s4_normalsNoise");
    shdrWobbleSphere.load("shaders/wobbleSphere");
    shdrBlurX.load("shaders/blurX");
    shdrBlurY.load("shaders/blurY");
    shdrRejuvenator.load("shaders/rejuvenator");

    // See https://threejs.org/docs/#api/en/constants/Textures

    ofFboSettings s1;
    s1.width = cfg->res.x;
    s1.height = cfg->res.y;
    s1.wrapModeHorizontal = GL_REPEAT;
    s1.wrapModeVertical = GL_REPEAT;
    s1.minFilter = GL_LINEAR;
    s1.maxFilter = GL_LINEAR;
    s1.internalformat = GL_RGBA16F;

    ofFboSettings s2(s1);
    s2.minFilter = GL_NEAREST;
    s2.maxFilter = GL_NEAREST;

    ofFboSettings s3(s1);
    s3.width = cfg->resNoise.x;
    s3.height = cfg->resNoise.y;
    s3.internalformat = GL_RGBA;

    ofFboSettings s4(s3);
    s4.internalformat = GL_RGBA16F;

    fboPositions.allocate(s1);
    fboNormals.allocate(s1);
    fboNormalsSmooth.allocate(s2);
    fboNormalsNoise.allocate(s3);

    fboWrinkles.allocate(s4);
    fboWrinklesA.allocate(s4);
    fboWrinklesB.allocate(s4);

    makeBlobGeometry();

    resetScratchs();
}

//--------------------------------------------------------------
void WobbleHandler::resetScratchs() {
    fboWrinkles.begin();
    ofClear(ofFloatColor(wrinkleBaseColor));
    fboWrinkles.end();

    fboWrinklesB.begin();
    fboWrinkles.draw(0, 0);
    fboWrinklesB.end();
}
//--------------------------------------------------------------
void WobbleHandler::makeScratch() {
    if(ofRandom(1.0) > gui->wrinkle_probability) {
        return;
    }
    auto w = fboWrinkles.getWidth();
    auto h = fboWrinkles.getHeight();
    auto rx = w * 0.02;
    auto ry = h * 0.02;
    auto x0 = w * ofRandom(0.0, 1.0);
    auto y0 = h * ofRandom(0.2, 0.8);
    auto x3 = x0 + w * ofRandom(0.0, 0.15);
    auto y3 = y0 + (y0 > h / 2 ? -1 : 1) *
        h * ofRandom(0.0, 0.15);
    auto x1 = glm::mix(x0, x3, 0.3) + ofRandom(-rx, rx) * 10;
    auto y1 = glm::mix(y0, y3, 0.3) + ofRandom(-ry, ry) * 10;
    auto x2 = glm::mix(x0, x3, 0.7) + ofRandom(-rx, rx) * 10;
    auto y2 = glm::mix(y0, y3, 0.7) + ofRandom(-ry, ry) * 10;

    auto num = ofRandom(1, 6);
    // Update wrinkles texture
    fboWrinkles.begin();
    ofNoFill();
    for(int i = 0; i < num; i++) {
        auto elevation = wrinkleBaseColor + ofRandom(-0.3, 0.03);
        float p[] = {
            x0 + ofRandom(-rx, rx),
            y0 + ofRandom(-ry, ry),
            x1 + ofRandom(-rx, rx),
            y1 + ofRandom(-ry, ry),
            x2 + ofRandom(-rx, rx),
            y2 + ofRandom(-ry, ry),
            x3 + ofRandom(-rx, rx),
            y3 + ofRandom(-ry, ry)
        };
        ofSetColor(ofFloatColor(elevation));
        ofDrawBezier(p[0], p[1], p[2], p[3], p[4], p[5], p[6], p[7]);
        // Draw a translated copy: [  --]-  ->  --[-   ]
        ofPushMatrix();
        ofTranslate(-w, 0);
        ofDrawBezier(p[0], p[1], p[2], p[3], p[4], p[5], p[6], p[7]);
        ofPopMatrix();
    }
    fboWrinkles.end();

    ofFill();
    ofSetColor(ofColor::white);
}

//--------------------------------------------------------------
void setUniforms(const ofShader & shader,
    const std::string & uniformName,
    const ofParameterGroup & group) {
    switch (group.size()) {
        case 2:
            shader.setUniform2f(uniformName,
                group.getFloat(0),
                group.getFloat(1));
            break;
        case 3:
            shader.setUniform3f(uniformName,
                group.getFloat(0),
                group.getFloat(1),
                group.getFloat(2));
            break;
        case 4:
            shader.setUniform4f(uniformName,
                group.getFloat(0),
                group.getFloat(1),
                group.getFloat(2),
                group.getFloat(3));
            break;
    }
}

//--------------------------------------------------------------
// setUniform4f exists in OF, but we need 3f (no alpha)
void setUniform3f(const ofShader & shader,
    const std::string & uniformName,
    const ofFloatColor & color) {
    shader.setUniform3f(uniformName, color.r, color.g, color.b);
}

//--------------------------------------------------------------
void WobbleHandler::update(double delta) {
    auto mouseButtonChanged =
        fabs(uniforms->mouseDownCurrent - uniforms->mouseDownTarget) > 0.001;

    if (mouseButtonChanged) {
        uniforms->mouseDownCurrent = glm::mix(
                uniforms->mouseDownCurrent,
                uniforms->mouseDownTarget,
                delta * 5.0);
    }

    if (isDragging) {
        uniforms->cursorWorldCurrent = glm::mix(
                uniforms->cursorWorldCurrent,
                uniforms->cursorWorldTarget,
                delta * 7.0);

        uniforms->avoidPositionCurrent = glm::mix(
                uniforms->avoidPositionCurrent,
                avoidPositionTarget,
                delta * 2.0);

        uniforms->attractPositionCurrent = glm::mix(
                uniforms->attractPositionCurrent,
                uniforms->cursorWorldTarget,
                delta * 2.0);
    }

    updatePositions();
    updateNormals();
    updateNormalsSmooth();
    updateNormalsNoise();
    updateScratches();
}

/**
 * Read  `fboWrinkles`
 * Use   `shdrRejuvenator`
 * Write `fboWrinklesB`
 *
 * then
 *
 * Read  `fboWrinklesB`
 * Write `fboWrinkles`
 */
void WobbleHandler::updateScratches() {
    auto & shdr = shdrRejuvenator;

    // fboWrinkles -> fboWrinklesB (using shdrRejuvenator)
    fboWrinklesB.begin();
    shdr.begin();
    shdr.setUniform1f("amount", gui->rejuvenator_amount);
    shdr.setUniform1f("speed", gui->rejuvenator_speed);
    fboWrinkles.draw(0, 0);
    shdr.end();
    fboWrinklesB.end();

    // fboWrinklesB -> fboWrinkles (copy)
    fboWrinkles.begin();
    fboWrinklesB.draw(0, 0);
    fboWrinkles.end();

    // Blur wrinkles texture

    // fboWrinkles -> fboWrinklesA (using shdrBlurX)
    fboWrinklesA.begin();
    shdrBlurX.begin();
    fboWrinkles.draw(0, 0);
    shdrBlurX.end();
    fboWrinklesA.end();

    // fboWrinklesA -> fboWrinklesB (using shdrBlurY)
    fboWrinklesB.begin();
    shdrBlurY.begin();
    fboWrinklesA.draw(0, 0);
    shdrBlurY.end();
    fboWrinklesB.end();
}

/**
 * Read  `fboNormalsSmooth`
 * Read  `fboWrinkles`
 * Use   `shdr1Positions`
 * Write `fboPositions`
 */
void WobbleHandler::updatePositions() {
    auto & shdr = shdr1Positions;
    fboPositions.begin();
    shdr.begin();
    shdr.setUniform1f("mouseInteraction", uniforms->mouseDownCurrent);
    shdr.setUniform2f("mousePos", uniforms->mousePos);
    shdr.setUniform4f("volumeMicrophone", uniforms->volumeMicrophone);
    shdr.setUniform4f("microphoneScaleSpectrums", gui->microphoneScaleSpectrums);
    shdr.setUniform1f("microphoneScaleStrength", gui->microphoneScaleStrength);
    shdr.setUniform4f("microphoneBigNoiseScaleSpectrums",
        gui->microphoneBigNoiseScaleSpectrums);
    setUniforms(shdr, "microphoneBigNoiseScaleRange",
        gui->microphoneBigNoiseScaleRange);
    shdr.setUniform4f("microphoneBigNoiseOffsetSpectrums",
        gui->microphoneBigNoiseOffsetSpectrums);
    setUniforms(shdr, "microphoneBigNoiseOffsetRange",
        gui->microphoneBigNoiseOffsetRange);
    shdr.setUniform4f("microphoneSmallNoiseScaleSpectrums",
        gui->microphoneSmallNoiseScaleSpectrums);
    setUniforms(shdr, "microphoneSmallNoiseScaleRange",
        gui->microphoneSmallNoiseScaleRange);
    shdr.setUniform4f("microphoneSmallNoiseOffsetSpectrums",
        gui->microphoneSmallNoiseOffsetSpectrums);
    setUniforms(shdr, "microphoneSmallNoiseOffsetRange",
        gui->microphoneSmallNoiseOffsetRange);
    shdr.setUniform4f("volumeSong", uniforms->volumeSong);
    shdr.setUniform4f("songScaleSpectrums", gui->songScaleSpectrums);
    shdr.setUniform1f("songScaleStrength", gui->songScaleStrength);
    shdr.setUniform4f("songBigNoiseScaleSpectrums",
        gui->songBigNoiseScaleSpectrums);
    setUniforms(shdr, "songBigNoiseScaleRange", gui->songBigNoiseScaleRange);
    shdr.setUniform4f("songBigNoiseOffsetSpectrums",
        gui->songBigNoiseOffsetSpectrums);
    setUniforms(shdr, "songBigNoiseOffsetRange", gui->songBigNoiseOffsetRange);
    shdr.setUniform4f("songSmallNoiseScaleSpectrums",
        gui->songSmallNoiseScaleSpectrums);
    setUniforms(shdr, "songSmallNoiseScaleRange", gui->songSmallNoiseScaleRange);
    shdr.setUniform4f("songSmallNoiseOffsetSpectrums",
        gui->songSmallNoiseOffsetSpectrums);
    setUniforms(shdr, "songSmallNoiseOffsetRange", gui->songSmallNoiseOffsetRange);
    shdr.setUniform3f("cursorWorldSmoothed", uniforms->cursorWorldCurrent);
    shdr.setUniform1f("yRotation", uniforms->yRotation);
    shdr.setUniform1f("positionZ", uniforms->positionZ);
    shdr.setUniform3f("shapeNoiseData",
        gui->shapeNoiseData_scale,
        uniforms->timeShapeNoise,
        gui->shapeNoiseData_strength
    );
    shdr.setUniform3f("bigNoiseData",
        gui->bigNoiseData_scale,
        uniforms->timeBigNoise,
        gui->bigNoiseData_strength
    );
    shdr.setUniform3f("smallNoiseData",
        gui->smallNoiseData_scale,
        uniforms->timeSmallNoise,
        gui->smallNoiseData_strength
    );
    setUniforms(shdr, "deflateNoiseGradientData", gui->deflateNoiseGradientData);
    shdr.setUniformTexture("smoothedNormalsMap", fboNormalsSmooth.getTexture(), 1);
    shdr.setUniformTexture("wrinklesMap", fboWrinklesB.getTexture(), 2);
    shdr.setUniform1f("aspectRatio", uniforms->aspectRatio);
    shdr.setUniform3f("attractPositionWorld", uniforms->attractPositionCurrent);
    shdr.setUniform1f("mouseInteraction", uniforms->mouseDownCurrent);
    shdr.setUniform3f("positionNoiseData",
        uniforms->timePositionNoise,
        gui->positionNoiseData_distance,
        gui->positionNoiseData_distanceZ
    );
    shdr.setUniform3f("avoidPositionWorld", uniforms->avoidPositionCurrent);
    shdr.setUniform3f("behaviourNoiseData",
        gui->behaviourNoiseData_center,
        uniforms->timeBehaviourNoise,
        gui->behaviourNoiseData_range
    );
    shdr.setUniform1f("positionZDistance", gui->positionZDistance);
    shdr.setUniform1f("positionZ", uniforms->positionZ);
    shdr.setUniform2f("deflateNoiseData",
        uniforms->timeDeflateNoise,
        gui->deflateNoiseData_edge
    );
    shdr.setUniform3f("audioReactivenessNoise",
        gui->audioReactivenessNoise_center,
        uniforms->timeAudioReactivenessNoise,
        gui->audioReactivenessNoise_range
    );
    area.draw();
    shdr.end();
    fboPositions.end();
}

/**
 * Read  `fboPositions`
 * Use   `shdr2Normals`
 * Write `fboNormals`
 */
void WobbleHandler::updateNormals() {
    auto & shdr = shdr2Normals;
    fboNormals.begin();
    shdr.begin();
    shdr.setUniformTexture("positionsMap", fboPositions.getTexture(), 1);
    shdr.setUniform3f("offset",
        1.0f / static_cast<float>(cfg->res.x),
        1.0f / static_cast<float>(cfg->res.y),
        0.0f);
    area.draw();
    shdr.end();
    fboNormals.end();
}

/**
 * Read  `fboNormals`
 * Use   `shdr3NormalsSmooth`
 * Write `fboNormalsSmooth`
 */
void WobbleHandler::updateNormalsSmooth() {
    auto & shdr = shdr3NormalsSmooth;
    fboNormalsSmooth.begin();
    shdr.begin();
    shdr.setUniform1f("normalProcessingSmoother", gui->normalProcessingSmoother);
    shdr.setUniformTexture("normalsMap", fboNormals.getTexture(), 1);
    shdr.setUniform3f("offset",
        1.0f / static_cast<float>(cfg->res.x),
        1.0f / static_cast<float>(cfg->res.y),
        0.0f);
    area.draw();
    shdr.end();
    fboNormalsSmooth.end();
}

/**
 * Read  `fboPositions`
 * Use   `shdr4NormalsNoise`
 * Write `fboNormalsNoise`
 */
void WobbleHandler::updateNormalsNoise() {
    auto & shdr = shdr4NormalsNoise;
    fboNormalsNoise.begin();
    shdr.begin();
    shdr.setUniformTexture("positionsMap", fboPositions.getTexture(), 1);

    shdr.setUniform1f("noiseNormalScale0", gui->noiseNormalScale0);
    shdr.setUniform1f("noiseNormalTime0", uniforms->timeNoiseNormal0);
    shdr.setUniform1f("noiseNormalStrength0", gui->noiseNormalStrength0);

    shdr.setUniform1f("noiseNormalScale1", gui->noiseNormalScale1);
    shdr.setUniform1f("noiseNormalTime1", uniforms->timeNoiseNormal1);
    shdr.setUniform1f("noiseNormalStrength1", gui->noiseNormalStrength1);

    areaNoise.draw();
    shdr.end();
    fboNormalsNoise.end();
}

/**
 * Read  `fboPositions`
 * Read  `fboNormalsSmooth`
 * Read  `fboNormalsNoise`
 * Use   `shdrWobbleSphere`
 * Write `cam`
 */
void WobbleHandler::render() {
    auto & shdr = shdrWobbleSphere;
    shdr.begin();
    cam->begin();

    shdr.setUniformTexture("positionsMap", fboPositions.getTexture(), 1);
    shdr.setUniformTexture("smoothedNormalsMap", fboNormalsSmooth.getTexture(), 2);
    shdr.setUniformTexture("noiseNormalMap", fboNormalsNoise.getTexture(), 3);

    shdr.setUniform1f("time", uniforms->time);
    shdr.setUniform1f("noiseNormalAmount", gui->noiseNormalAmount);
    shdr.setUniform1f("normalNoiseDistance", gui->normalNoiseDistance);
    shdr.setUniform1f("rimPower", gui->rimPower);
    shdr.setUniform1f("rimScale", gui->rimScale);
    shdr.setUniform1f("spotPower", gui->spotPower);
    shdr.setUniform1f("spotScale", gui->spotScale);
    shdr.setUniform1f("aaPower", gui->aaPower);
    shdr.setUniform1f("aaScale", gui->aaScale);
    shdr.setUniform1f("opacity", gui->opacity);

    setUniform3f(shdr, "baseColor", gui->baseColor);
    setUniform3f(shdr, "rimColor",  gui->rimColor);
    setUniform3f(shdr, "spotColor", gui->spotColor);
    shdr.setUniform3f("spotVector", gui->spotVector);
    setUniform3f(shdr, "backgroundColor", gui->backgroundColor);

    ofSetDepthTest(true);
    mesh.draw();
    ofSetDepthTest(false);

    cam->end();
    shdr.end();
}

//--------------------------------------------------------------
void WobbleHandler::draw(bool debug) {
    render();

    if(debug) {
        auto side = 200;
        auto margin = 10;
        auto mside = margin + side;

        ofFill();
        ofSetColor(ofColor(255, 100));
        ofDrawRectangle(0, 0, mside * 3 + margin, mside + margin);
        ofDrawRectangle(0, mside + margin, mside + margin, mside * 2);

        ofSetColor(ofColor(255));
        fboPositions.draw(margin, margin, side, side);
        fboNormals.draw(mside + margin, margin, side, side);
        fboNormalsSmooth.draw(mside * 2 + margin, margin, side, side);
        fboNormalsNoise.draw(margin, mside + margin, side, side);
        fboWrinklesB.draw(margin, mside * 2 + margin, side, side);
    }
}

//--------------------------------------------------------------
void WobbleHandler::updateMouseTargets() {
    uniforms->cursorWorldTarget.x = uniforms->mousePos.x * 1.0f;
    uniforms->cursorWorldTarget.y = uniforms->mousePos.y * 1.0f;

    avoidPositionTarget.x =
        glm::clamp(-uniforms->cursorWorldTarget.x * 9999.9f, -0.5f, 0.5f);
    avoidPositionTarget.y =
        glm::clamp(-uniforms->cursorWorldTarget.y * 9999.9f, -0.5f, 0.5f);
}

//--------------------------------------------------------------
void WobbleHandler::onMouseDown() {
    updateMouseTargets();
    uniforms->cursorWorldCurrent = uniforms->cursorWorldTarget;
    uniforms->avoidPositionCurrent = avoidPositionTarget;
    uniforms->attractPositionCurrent *= 0.0;
    isDragging = true;
}

//--------------------------------------------------------------
void WobbleHandler::onDrag() {
    updateMouseTargets();
}

//--------------------------------------------------------------
void WobbleHandler::onMouseUp() {
    isDragging = false;
}
