#pragma once

#include "ofxGui.h"
#include "ofxDropdown.h"

// See
// https://forum.openframeworks.cc/t/new-add-on-ofxsurfingsmooth-easy-smooth-ofparameters/37825
// for smoothing ofParameters

class Gui {
    private:

    public:
        void setup();
        void draw();
        void changeSong(int indexDelta);

        ofParameterGroup audioFile, audioLive;

        ofParameter<float> yRotationSpeed { "Y Rotation Speed", -0.05f, -0.10f, 0.10f };

        // ---
        ofParameter<float> audioReactivenessNoise_center { "Center", 0.90f, -2.00f, 2.00f };
        ofParameter<float> audioReactivenessNoise_speed { "Speed", 0.80f, -20.0f, 20.0f };
        ofParameter<float> audioReactivenessNoise_range { "Range", 0.30f, 0.0f, 2.00f };
        ofParameterGroup audioReactivenessNoise {
            "Audio Reactiveness Noise",
            audioReactivenessNoise_center,
            audioReactivenessNoise_speed,
            audioReactivenessNoise_range
        };

        // ---
        ofParameter<float> movementSongMinValue { "Movement Min Value", 0.10f, 0.0f, 1.00f };
        ofParameter<float> movementMicrophoneMinValue { "Movement Min Value", 0.20f, 0.0f, 1.00f };

        // ---
        ofParameter<float> movementAudioModifier_speedWithSound { "Speed With Sound", 0.50f, 0.0f, 2.00f };
        ofParameter<float> movementAudioModifier_speedWithoutSound { "Speed Without Sound", 0.06f, 0.0f, 2.00f };
        ofParameter<float> movementAudioModifier_fadeSpeedUp { "Fade Speed Up", 4.00f, 0.0f, 5.00f };
        ofParameter<float> movementAudioModifier_fadeSpeedDown { "Fade Speed Down", 2.00f, 0.0f, 5.00f };
        ofParameterGroup movementAudioModifier {
            "Movement Audio Modifier",
            movementAudioModifier_speedWithSound,
            movementAudioModifier_speedWithoutSound,
            movementAudioModifier_fadeSpeedUp,
            movementAudioModifier_fadeSpeedDown
        };

        // ---
        ofParameter<float> slowdownMinDuration { "Min", 1.00f, 0.0f, 100.00f };
        ofParameter<float> slowdownMaxDuration { "Max", 15.10f, 0.0f, 100.00f };
        ofParameterGroup slowdownDuration {
            "Slowdown Duration",
            slowdownMinDuration,
            slowdownMaxDuration
        };

        // ---
        ofParameter<float> slowedDownMinDuration { "Min", 1.00f, 0.0f, 100.00f };
        ofParameter<float> slowedDownMaxDuration { "Max", 4.00f, 0.0f, 100.00f };
        ofParameterGroup slowedDownDuration {
            "Slowed Down Duration",
            slowedDownMinDuration,
            slowedDownMaxDuration
        };

        // ---
        ofParameter<float> slowdownScale { "Slowdown Scale", 0.30f, 0.0f, 100.00f };

        // ---
        ofParameter<float> speedUpMinDuration { "Min", 7.00f, 0.0f, 100.00f };
        ofParameter<float> speedUpMaxDuration { "Max", 30.10f, 0.0f, 100.00f };
        ofParameterGroup speedUpDuration {
            "Speedup Duration",
            speedUpMinDuration,
            speedUpMaxDuration
        };

        // ---
        ofParameter<float> spedUpMinDuration { "Min", 0.50f, 0.0f, 100.00f };
        ofParameter<float> spedUpMaxDuration { "Max", 2.50f, 0.0f, 100.00f };
        ofParameterGroup spedUpDuration {
            "Sped up Duration",
            spedUpMinDuration,
            spedUpMaxDuration
        };

        // ---
        ofParameter<float> shapeSongMinValue { "Shape Min Value", 0.10f, 0.0f, 1.00f };
        ofParameter<float> shapeMicrophoneMinValue { "Shape Min Value", 0.20f, 0.0f, 1.00f };

        // ---
        ofParameter<float> shapeAudioModifier_speedWithSound { "Speed With Sound", 0.70f, 0.0f, 2.00f };
        ofParameter<float> shapeAudioModifier_speedWithoutSound { "Speed Without Sound", 0.06f, 0.0f, 2.00f};
        ofParameter<float> shapeAudioModifier_fadeSpeedUp { "Fade Speed Up", 4.00f, 0.0f, 5.00f };
        ofParameter<float> shapeAudioModifier_fadeSpeedDown { "Fade Speed Down", 0.80f, 0.0f, 5.00f };
        ofParameterGroup shapeAudioModifier {
            "Shape Audio Modifier",
            shapeAudioModifier_speedWithSound,
            shapeAudioModifier_speedWithoutSound,
            shapeAudioModifier_fadeSpeedUp,
            shapeAudioModifier_fadeSpeedDown
        };

        // ---
        ofParameter<float> positionZDistance { "Position Z Distance", 15.00f, 0.0f, 20.00f };

        // ---
        ofParameter<float> behaviourNoiseData_center { "Center", 0.10f, 0.0f, 5.00f };
        ofParameter<float> behaviourNoiseData_speed { "Speed", 0.10f, -5.0f, 5.00f };
        ofParameter<float> behaviourNoiseData_range { "Range", 0.50f, 0.0f, 10.00f };
        ofParameterGroup behaviourNoiseData {
            "Behaviour Noise Data",
            behaviourNoiseData_center,
            behaviourNoiseData_speed,
            behaviourNoiseData_range
        };

        // ---
        ofParameter<glm::vec4> microphoneScaleSpectrums {
            "Scale Spectrums",
            glm::vec4(1.00f, 1.00f, 1.00f, 1.00f),
            glm::vec4(0.0f),
            glm::vec4(3.00f)
        };

        ofParameter<float> microphoneScaleStrength { "Scale Strength", 0.60f, 0.0f, 2.00f };
        ofParameter<float> microphoneFadeSpeed { "Fade Speed", 2.00f, 0.0f, 10.00f };
        ofParameter<float> microphoneVolumeScaler { "Volume Scaler", 1.00f, 0.0f, 5.00f };
        ofParameter<float> microphoneSpectrumSmoothing { "Spectrum smoothing", 0.5f, 0.0f, 1.0f };

        // -- microphone big noise

        ofParameter<glm::vec4> microphoneBigNoiseScaleSpectrums {
            "Scale Spectrums",
            glm::vec4(1.00f, 0.20f, 0.20f, 0.20f),
            glm::vec4(0.0f),
            glm::vec4(3.00f)
        };

        ofParameter<float> microphoneBigNoiseScaleRange_min { "Min", 1.00f, 0.0f, 20.00f };
        ofParameter<float> microphoneBigNoiseScaleRange_max { "Max", 1.20f, 0.0f, 5.00f };
        ofParameterGroup microphoneBigNoiseScaleRange {
            "Scale Range",
            microphoneBigNoiseScaleRange_min,
            microphoneBigNoiseScaleRange_max
        };

        ofParameter<glm::vec4> microphoneBigNoiseOffsetSpectrums {
            "Offset Spectrums",
            glm::vec4(0.20f, 1.00f, 0.20f, 0.20f),
            glm::vec4(0.0f),
            glm::vec4(3.00f)
        };

        ofParameter<float> microphoneBigNoiseOffsetRange_min { "Min", 1.00f, 0.0f, 20.00f };
        ofParameter<float> microphoneBigNoiseOffsetRange_max { "Max", 1.20f, 0.0f, 5.00f };
        ofParameterGroup microphoneBigNoiseOffsetRange {
            "Offset Range",
            microphoneBigNoiseOffsetRange_min,
            microphoneBigNoiseOffsetRange_max
        };

        // ---
        ofParameterGroup microphoneBigNoise {
            "Big Noise",
            microphoneBigNoiseScaleSpectrums,
            microphoneBigNoiseScaleRange,
            microphoneBigNoiseOffsetSpectrums,
            microphoneBigNoiseOffsetRange,
        };

        // -- microphone small noise

        ofParameter<glm::vec4> microphoneSmallNoiseScaleSpectrums {
            "Scale Spectrums",
            glm::vec4(0.20f, 0.20f, 0.20f, 1.00f),
            glm::vec4(0.0f),
            glm::vec4(3.00f)
        };

        ofParameter<float> microphoneSmallNoiseScaleRange_min { "Min", 1.00f, 0.0f, 20.00f };
        ofParameter<float> microphoneSmallNoiseScaleRange_max { "Max", 1.20f, 0.0f, 5.00f };
        ofParameterGroup microphoneSmallNoiseScaleRange {
            "Scale Range",
            microphoneSmallNoiseScaleRange_min,
            microphoneSmallNoiseScaleRange_max
        };

        ofParameter<glm::vec4> microphoneSmallNoiseOffsetSpectrums {
            "Offset Spectrums",
            glm::vec4(0.20f, 0.20f, 1.00f, 0.20f),
            glm::vec4(0.0f),
            glm::vec4(3.00f)
        };

        ofParameter<float> microphoneSmallNoiseOffsetRange_min { "Min", 1.00f, 0.0f, 20.00f };
        ofParameter<float> microphoneSmallNoiseOffsetRange_max { "Max", 1.20f, 0.0f, 5.00f };
        ofParameterGroup microphoneSmallNoiseOffsetRange {
            "Offset Range",
            microphoneSmallNoiseOffsetRange_min,
            microphoneSmallNoiseOffsetRange_max
        };

        // ---
        ofParameterGroup microphoneSmallNoise {
            "Small Noise",
            microphoneSmallNoiseScaleSpectrums,
            microphoneSmallNoiseScaleRange,
            microphoneSmallNoiseOffsetSpectrums,
            microphoneSmallNoiseOffsetRange,
        };

        // --

        ofParameter<float> songFadeSpeed { "Fade Speed", 2.00f, 0.0f, 10.00f };
        ofParameter<float> songVolumeScaler { "Volume Scaler", 1.00f, 0.0f, 5.00f };
        ofParameter<float> songSpectrumSmoothing { "Spectrum smoothing", 0.5f, 0.0f, 1.0f };

        ofParameter<glm::vec4> songScaleSpectrums {
            "Scale Spectrums",
            glm::vec4(1.00f, 1.00f, 1.00f, 1.00f),
            glm::vec4(0.0f),
            glm::vec4(3.00f)
        };

        ofParameter<float> songScaleStrength { "Scale Strength", 0.10f, 0.0f, 2.00f };

        // -- song big noise

        ofParameter<glm::vec4> songBigNoiseScaleSpectrums {
            "Scale Spectrums",
            glm::vec4(1.00f, 0.20f, 0.20f, 0.20f),
            glm::vec4(0.0f),
            glm::vec4(3.00f)
        };

        // ---
        ofParameter<float> songBigNoiseScaleRange_min { "Min", 0.90f, 0.0f, 20.00f };
        ofParameter<float> songBigNoiseScaleRange_max { "Max", 1.50f, 0.0f, 5.00f };
        ofParameterGroup songBigNoiseScaleRange {
            "Scale Range",
            songBigNoiseScaleRange_min,
            songBigNoiseScaleRange_max
        };

        // ---
        ofParameter<glm::vec4> songBigNoiseOffsetSpectrums {
            "Offset Spectrums",
            glm::vec4(0.20f, 1.00f, 0.20f, 0.20f),
            glm::vec4(0.0f),
            glm::vec4(3.00f)
        };

        // ---
        ofParameter<float> songBigNoiseOffsetRange_min { "Min", 0.90f, 0.0f, 20.00f };
        ofParameter<float> songBigNoiseOffsetRange_max { "Max", 1.20f, 0.0f, 5.00f };
        ofParameterGroup songBigNoiseOffsetRange {
            "Offset Range",
            songBigNoiseOffsetRange_min,
            songBigNoiseOffsetRange_max
        };

        // ---
        ofParameterGroup songBigNoise {
            "Big Noise",
            songBigNoiseScaleSpectrums,
            songBigNoiseScaleRange,
            songBigNoiseOffsetSpectrums,
            songBigNoiseOffsetRange,
        };


        // -- song small noise

        ofParameter<glm::vec4> songSmallNoiseScaleSpectrums {
            "Scale Spectrums",
            glm::vec4(0.20f, 0.20f, 1.00f, 0.20f),
            glm::vec4(0.0f),
            glm::vec4(3.00f)
        };

        // ---
        ofParameter<float> songSmallNoiseScaleRange_min { "Min", 0.90f, 0.0f, 20.00f };
        ofParameter<float> songSmallNoiseScaleRange_max { "Max", 1.50f, 0.0f, 5.00f };
        ofParameterGroup songSmallNoiseScaleRange {
            "Scale Range",
            songSmallNoiseScaleRange_min,
            songSmallNoiseScaleRange_max
        };

        // ---
        ofParameter<glm::vec4> songSmallNoiseOffsetSpectrums {
            "Offset Spectrums",
            glm::vec4(0.20f, 0.20f, 0.20f, 1.00f),
            glm::vec4(0.0f),
            glm::vec4(3.00f)
        };

        // ---
        ofParameter<float> songSmallNoiseOffsetRange_min { "Min", 0.90f, 0.0f, 20.00f };
        ofParameter<float> songSmallNoiseOffsetRange_max { "Max", 1.20f, 0.0f, 5.00f };
        ofParameterGroup songSmallNoiseOffsetRange {
            "Offset Range",
            songSmallNoiseOffsetRange_min,
            songSmallNoiseOffsetRange_max
        };

        // ---
        ofParameterGroup songSmallNoise {
            "Small Noise",
            songSmallNoiseScaleSpectrums,
            songSmallNoiseScaleRange,
            songSmallNoiseOffsetSpectrums,
            songSmallNoiseOffsetRange,
        };

        ofxDropdown_<ofFile> songDropdown { "Song" };

        // --

        ofParameter<float> deflateNoiseData_speed { "Speed", 0.05f, -5.0f, 5.0f };
        ofParameter<float> deflateNoiseData_edge { "Edge", 0.40f, 0.0f, 5.00f };
        ofParameterGroup deflateNoiseData {
            "Deflate Noise Data",
            deflateNoiseData_speed,
            deflateNoiseData_edge
        };

        // --

        ofParameter<float> rejuvenator_speed { "Recover speed", 0.001f, 0.0f, 0.005f };
        ofParameter<float> rejuvenator_amount { "Recover amount", 0.50f, 0.0f, 1.0f };
        ofParameter<float> wrinkle_probability { "Wrinkle probability", 0.0f, 0.0f, 1.0f };
        ofParameter<float> wrinkle_soundSens { "Sound sensitivity", 0.0f, 0.0f, 1.0f };
        ofParameterGroup rejuvenator {
            "Aging",
            rejuvenator_speed,
            rejuvenator_amount,
            wrinkle_probability,
            wrinkle_soundSens
        };

        // ---
        ofParameter<float> deflateNoiseGradientData_scale { "Scale", 0.57f, 0.0f, 5.00f };
        ofParameter<float> deflateNoiseGradientData_strength { "Strength", 0.03f, 0.0f, 1.00f };
        ofParameter<float> deflateNoiseGradientData_strengthGrad { "Strength Grad", 0.25f, 0.0f, 5.00f };
        ofParameter<float> deflateNoiseGradientData_scaleDown { "Scale Down", 0.08f, -1.00f, 1.00f };
        ofParameterGroup deflateNoiseGradientData {
            "Deflate Noise Gradient Data",
            deflateNoiseGradientData_scale,
            deflateNoiseGradientData_strength,
            deflateNoiseGradientData_strengthGrad,
            deflateNoiseGradientData_scaleDown
        };

        // ---
        ofParameter<float> positionNoiseData_speed { "Speed", 0.02f, -5.0f, 5.0f };
        ofParameter<float> positionNoiseData_distance { "Distance", 0.40f, 0.0f, 10.00f };
        ofParameter<float> positionNoiseData_distanceZ { "Distance Z", 0.05f, 0.0f, 5.00f };
        ofParameterGroup positionNoiseData {
            "Position Noise Data",
            positionNoiseData_speed,
            positionNoiseData_distance,
            positionNoiseData_distanceZ
        };

        // ---
        ofParameter<float> shapeNoiseData_scale { "Scale", 0.20f, 0.0f, 1.00f };
        ofParameter<float> shapeNoiseData_speed { "Speed", 0.20f, -5.0f, 5.0f };
        ofParameter<float> shapeNoiseData_strength { "Strength", 0.05f, 0.0f, 1.00f };
        ofParameterGroup shapeNoiseData {
            "Shape Noise Data",
            shapeNoiseData_scale,
            shapeNoiseData_speed,
            shapeNoiseData_strength
        };

        // ---
        ofParameter<float> bigNoiseData_scale { "Scale", 1.00f, 0.0f, 4.00f };
        ofParameter<float> bigNoiseData_speed { "Speed", 0.30f, -5.0f, 5.0f };
        ofParameter<float> bigNoiseData_strength { "Strength", 0.10f, 0.0f, 0.80f };
        ofParameterGroup bigNoiseData {
            "Big Noise Data",
            bigNoiseData_scale,
            bigNoiseData_speed,
            bigNoiseData_strength
        };

        // ---
        ofParameter<float> smallNoiseData_scale { "Scale", 4.50f, 0.0f, 20.00f };
        ofParameter<float> smallNoiseData_speed { "Speed", 0.30f, -5.0f, 5.0f };
        ofParameter<float> smallNoiseData_strength { "Strength", 0.02f, 0.0f, 0.40f };
        ofParameterGroup smallNoiseData {
            "Small Noise Data",
            smallNoiseData_scale,
            smallNoiseData_speed,
            smallNoiseData_strength
        };

        // ---
        ofParameter<float> normalProcessingSmoother { "Processing Smoother", 1.75f, 0.0f, 4.00f };
        ofParameter<float> noiseNormalAmount { "Noise Amount", 0.20f, 0.0f, 2.00f };
        ofParameter<float> normalNoiseDistance { "Noise Distance", 0.00f, 0.00f, 0.01f };

        // ---
        ofParameter<float> noiseNormalScale0 { "Scale", 2.00f, 0.00f, 40.00f };
        ofParameter<float> noiseNormalSpeed0 { "Speed", 0.40f, -20.0f, 20.0f };
        ofParameter<float> noiseNormalStrength0 { "Strength", 4.00f, 0.00f, 20.00f };
        ofParameterGroup normalNoise0 {
            "Noise 0",
            noiseNormalScale0,
            noiseNormalSpeed0,
            noiseNormalStrength0
        };

        // ---
        ofParameter<float> noiseNormalScale1 { "Scale", 11.00f, 0.00f, 40.00f };
        ofParameter<float> noiseNormalSpeed1 { "Speed", 1.00f, -20.0f, 20.0f };
        ofParameter<float> noiseNormalStrength1 { "Strength", 2.00f, 0.00f, 20.00f };
        ofParameterGroup normalNoise1 {
            "Noise 1",
            noiseNormalScale1,
            noiseNormalSpeed1,
            noiseNormalStrength1
        };

        // ---
        ofParameter<ofFloatColor> backgroundColor {
            "Background Color",
            ofFloatColor(0.004f),
            ofFloatColor(0.0f),
            ofFloatColor(1.00f)
        };

        // ---
        ofParameter<ofFloatColor> baseColor {
            "Base Color",
            ofFloatColor(0.30f, 0.30f, 0.30f),
            ofFloatColor(0.0f),
            ofFloatColor(1.00f)
        };

        // ---
        ofParameter<ofFloatColor> rimColor {
            "Color",
            ofFloatColor(0.50f, 0.50f, 0.50f),
            ofFloatColor(0.0f),
            ofFloatColor(1.00f)
        };
        ofParameter<float> rimPower { "Power", 2.50f, 0.00f, 20.00f };
        ofParameter<float> rimScale { "Scale", 1.40f, 0.00f, 20.00f };
        ofParameterGroup colorRim {
            "Rim",
            rimColor,
            rimPower,
            rimScale
        };

        // ---
        ofParameter<ofFloatColor> spotColor {
            "Color",
            ofFloatColor(0.30f, 0.30f, 0.30f),
            ofFloatColor(0.0f),
            ofFloatColor(1.00f)
        };
        ofParameter<glm::vec3> spotVector {
            "Vector",
            glm::vec3(0.06f, 0.48f, 0.16f),
            glm::vec3(-1.00f),
            glm::vec3(1.00f)
        };
        ofParameter<float> spotPower { "Power", 4.00f, 0.00f, 20.00f };
        ofParameter<float> spotScale { "Scale", 1.00f, 0.00f, 20.00f };
        ofParameterGroup colorSpot {
            "Spot",
            spotColor,
            spotVector,
            spotPower,
            spotScale
        };

        // ---
        ofParameter<float> opacity { "Opacity", 1.0f, 0.0f, 1.0f };
        ofParameter<float> aaPower { "Antialias Power", 12.00f, 0.00f, 20.00f };
        ofParameter<float> aaScale { "Antialias Scale", 10.00f, 0.00f, 20.00f };

        ofParameter<bool> debug { "debug", true };

        ofxPanel colCore,
                 colMicrophone,
                 colSong,
                 colShape,
                 colNormals,
                 colColor;
};
