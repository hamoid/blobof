#pragma once

#include "glm/ext/vector_int2.hpp"
#include <ofxCereal.h>

using namespace ofxCereal;

struct BlobConfig {
    glm::ivec2 res { 256, 256 };
    glm::ivec2 resNoise { 1024, 1024 };
    glm::ivec2 winSize { 800, 800 };
    glm::ivec2 winPos { 1, 1 }; // 0, 0 = centered
    glm::ivec2 uiSize { 1240, 1050 };
    glm::ivec2 uiPos { 800, 0 };
    glm::ivec2 glVersion { 3, 3 };
    bool multiWindow { true };
    bool verticalSync { true };
    int audioInputSampleRate { 44100 };
    int audioInputNumChannels { 1 };

    OFX_CEREAL_DEFINE(
        CEREAL_NVP(res),
        CEREAL_NVP(resNoise),
        CEREAL_NVP(winSize), CEREAL_NVP(winPos),
        CEREAL_NVP(uiSize), CEREAL_NVP(uiPos),
        CEREAL_NVP(glVersion),
        CEREAL_NVP(multiWindow),
        CEREAL_NVP(verticalSync),
        CEREAL_NVP(audioInputSampleRate),
        CEREAL_NVP(audioInputNumChannels)
    )
};
