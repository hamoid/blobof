#pragma once

#include "config.h"
#include "ofxAudioAnalyzer.h"

class AudioLive {
    private:
        shared_ptr<BlobConfig> cfg;
        ofSoundStream stream;
        ofSoundStreamSettings settings;
        ofxAudioAnalyzer analyzer;

        float average(vector<float> & data, size_t a, size_t b);
        float volumeCurr { 0.1f };
        float volumeNext { 0.1f };

    public:
        void setup(shared_ptr<BlobConfig> cfg);
        void update(float smooth);
        void exit();
        void setVolume(float v);
        void audioIn(ofSoundBuffer & inBuffer);
        bool anyBandAbove(float threshold);

        float rms;
        float bandMaxValue;
        vector<float> spectrum;

        glm::vec4 bands { 0.0 };
};

