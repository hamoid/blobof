#pragma once

#include "ofMathConstants.h"
#include "ofMath.h"
#include <set>

class Interpolator {
    private:
        static std::set<Interpolator *> instances;

        // Suggested:
        // - In pixels { 0.5, 10.0, 200.0 }
        // - Normalized { 0.01, 0.05, 0.1 }
        double maxAcceleration { 0.000025 }; // 0.008
        double maxSpeed { 0.002}; // 0.0001
        double dampDist { 0.25 }; // 0.25

        double targetValue { 0.0 };
        double currentValue { 0.0 };
        double currentSpeed { 0.0 };

    public:
        static void updateAll() {
            for(auto & i : instances) {
                i->update();
            }
        }
        Interpolator(double _currentValue,
                     double _maxAcceleration,
                     double _maxSpeed,
                     double _dampDist) {
            currentValue = _currentValue;
            targetValue = _currentValue;
            maxAcceleration = _maxAcceleration;
            maxSpeed = _maxSpeed;
            dampDist = _dampDist;

            instances.insert(this);
        }

        Interpolator(double _maxAcceleration,
                     double _maxSpeed,
                     double _dampDist) {
            maxAcceleration = _maxAcceleration;
            maxSpeed = _maxSpeed;
            dampDist = _dampDist;

            instances.insert(this);
        }

        ~Interpolator() {
            instances.erase(this);
        }

        void update() {
            auto offset = targetValue - currentValue;

            auto d = MIN(dampDist, std::abs(offset));

            offset = (offset < 0.0 ? -1.0 : 1.0) *
                    maxSpeed * d / dampDist;

            auto acceleration =
                CLAMP(offset - currentSpeed, -maxAcceleration, maxAcceleration);

            currentSpeed += acceleration;
            currentSpeed = CLAMP(currentSpeed, -maxSpeed, maxSpeed);

            currentValue += currentSpeed;
        }

        operator float() {
            return static_cast<float>(currentValue);
        }
        operator double() {
            return currentValue;
        }
        int operator<(double num) {
            return currentValue < num;
        }
        constexpr float operator*(float num) const {
            return static_cast<float>(currentValue) * num;
        }
        constexpr double operator*(double num) const {
            return currentValue * num;
        }
        void operator=(double target) {
            currentSpeed = 0.0;
            currentValue = target;
            targetValue = target;
        }
        void operator>>(double target) {
            targetValue = target;
        }
        void operator>>(int target) {
            targetValue = target;
        }
};

