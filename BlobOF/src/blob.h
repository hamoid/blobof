#pragma once

#include "audiolive.h"
#include "audiofile.h"
#include "interpolator.h"
#include "gui.h"
#include "wobblehandler.h"
#include "uniforms.h"
#include <ofxMidiIn.h>

class Blob : public ofBaseApp, public ofxMidiListener {
    private:
        shared_ptr<BlobConfig> cfg;
        void drawDebug();

    public:
        void setup() override;
        void setupGui(shared_ptr<BlobConfig> cfg);
        void update() override;
        void draw() override;
        void exit() override;
        void drawGui(ofEventArgs & args);
        void songChanged(ofFile & file);
        void toggleGUI();
        void toggleZDistance();
        void toggleRotation();
        void mousePressed(int x, int y, int button) override;
        void mouseDragged(int x, int y, int button) override;
        void mouseReleased(int x, int y, int button) override;
        void mouseExited(int x, int y) override;
        void keyPressed(int key) override;
        void windowResized(int w, int h) override;
        //void mouseMoved(int x, int y );
        //void keyReleased(int key);
        //void mouseEntered(int x, int y);
        //void dragEvent(ofDragInfo dragInfo);
        //void gotMessage(ofMessage msg);
        void newMidiMessage(ofxMidiMessage & eventArgs) override;

        // #region mouse data
        ofRectangle rect;
        glm::vec2 pointerStartPositionAbs { 0.0 };
        glm::vec2 pointerNormDragDelta { 0.0 };
        bool isDragging { false };
        bool isClick { false };
        size_t maxMouseClickTime { 0 };
        // #endregion

        // #region three.js
        float deltaCounter { 9999.9f };
        shared_ptr<ofCamera> cam = make_shared<ofCamera>();
        shared_ptr<Uniforms> uniforms = make_shared<Uniforms>();
        shared_ptr<Gui> gui = make_shared<Gui>();
        // #endregion

        // MIDI
        ofxMidiIn midiIn;
        ofxMidiMessage midiMessage;

        float movementTimeDeltaMultiplier { 1.0f };
        float shapeTimeDeltaMultiplier { 1.0f };
        float slowdownSpeed { 1.0f };
        Interpolator rotationSpeed { 1.0, 0.01, 0.05, 0.1 };

        AudioFile audioFile;
        AudioLive audioLive;

        float slowdownTime { 0.0 };
        float speedUpTime { 0.0 };
        bool slowdownToggle = false;
        bool speedUpToggle = false;
        bool guiVisible = true;

        WobbleHandler wobbleHandler;
};
