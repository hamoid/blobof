#include "blob.h"

//--------------------------------------------------------------
void Blob::setupGui(shared_ptr<BlobConfig> cfg) {
    this->cfg = cfg;
    gui->setup();
    gui->songDropdown.addListener(this, &Blob::songChanged);
}

//--------------------------------------------------------------
void Blob::setup() {
    ofSetVerticalSync(cfg->verticalSync);
    ofSetFrameRate(60);

    // GL_REPEAT (for texture wrap) requires NON-ARB textures
    ofDisableArbTex();

    audioFile.setup(cfg);
    audioLive.setup(cfg);


#if defined( TARGET_OSX )
    auto midiPorts = midiIn.getInPortList();
#else
    auto midiPorts = midiIn.getPortList();
#endif
    for(auto & portName : midiPorts) {
        if(portName.find("nanoKONTROL2") != std::string::npos) {
            std::cout << "connecting to " << portName << std::endl;
            midiIn.openPort(portName);
            midiIn.addListener(this);
            break;
        }
    }

    cam->setFov(50.0f);
    cam->setNearClip(1.0f);
    cam->setFarClip(100.0f);
    cam->setPosition(glm::vec3(0.0, 0.0, 4.0));
    cam->lookAt(glm::vec3(0.0));

    wobbleHandler.setup(uniforms, gui, cam, cfg);

    // Load first song automatically
    gui->changeSong(0);

    windowResized(ofGetWidth(), ofGetHeight());
}

//--------------------------------------------------------------
void Blob::newMidiMessage(ofxMidiMessage & msg) {
    midiMessage = msg;
    // # Korg nanoKONTROL2
    if(msg.control <= 23) { // Knobs / sliders
        auto val = msg.value / 127.0;
        switch(msg.control) {
            case 0: // slider 1
                gui->shapeNoiseData_scale = val;
                break;
            case 1: // slider 2
                gui->shapeNoiseData_strength = val;
                break;
            case 2: // slider 3
                gui->wrinkle_probability = val;
                break;
            case 3: // slider 4
                gui->wrinkle_soundSens = val;
                break;
            case 4: // slider 5
                gui->songVolumeScaler = val * 5.0f;
                break;
            case 5: // slider 6
                gui->songFadeSpeed = val * 10.0f;
                break;
            case 6: // slider 7
                gui->microphoneVolumeScaler = val * 5.0f;
                break;
            case 7: // slider 8
                gui->microphoneFadeSpeed = val * 10.0f;
                break;

            case 16: // Knob 1
                gui->opacity = val;
                break;
            case 17: { // knob 2
                float _, saturation, brightness;
                gui->baseColor->getHsb(_, saturation, brightness);
                gui->baseColor = ofFloatColor::fromHsb(val, saturation, brightness);
                break;
            }
            case 18: { // knob 3
                float hue, _, brightness;
                gui->baseColor->getHsb(hue, _, brightness);
                gui->baseColor = ofFloatColor::fromHsb(hue, val, brightness);
                break;
            }
            case 19: { // knob 4
                float _, saturation, brightness;
                gui->rimColor->getHsb(_, saturation, brightness);
                gui->rimColor = ofFloatColor::fromHsb(val, saturation, brightness);
                break;
            }
            case 20: {// knob 5
                float hue, _, brightness;
                gui->rimColor->getHsb(hue, _, brightness);
                gui->rimColor = ofFloatColor::fromHsb(hue, val, brightness);
                break;
            }
            //case 21: // knob 6
            //break;
            case 22: // Knob 7
                audioFile.setVolume(val);
                break;
            case 23: // Knob 8
                audioLive.setVolume(val);
                break;
            default:
                std::cout << msg.control << ": " << val << std::endl;
        }
    } else { // Buttons
        // 58 ⏴   59 ⏵
        // 46 cycle     60 set   61 ⏴   62 ⏵
        // 43 ⏪  44 ⏩  42 ⏹    41 ⏵   45 ⚫
        // 32..39 S
        // 48..55 M
        // 64..71 R
        if(msg.value > 64) {
            switch(msg.control) {
                case 42: // ⏹
                    audioFile.pause();
                    break;
                case 41: // ⏵
                    audioFile.play();
                    break;
                case 46: // cycle
                    gui->debug = !gui->debug;
                    break;
                case 58: // Track ⏴
                    gui->changeSong(-1);
                    break;
                case 59: // Track ⏵
                    gui->changeSong(1);
                    break;
                default:
                    std::cout << "Button " << msg.control << " down" << std::endl;
            }
        }
    }
}

//--------------------------------------------------------------
void Blob::songChanged(ofFile & file) {
    audioFile.load(file);
}


//--------------------------------------------------------------
void Blob::toggleGUI() {
    guiVisible ^= true;
}

//--------------------------------------------------------------
void Blob::toggleZDistance() {
    uniforms->positionZ >> (uniforms->positionZ < -0.5 ? 0.0 : -1.0);
}

//--------------------------------------------------------------
void Blob::toggleRotation() {
    rotationSpeed >> (rotationSpeed < 0.5 ? 1.0 : 0.0);
}

//--------------------------------------------------------------
void Blob::keyPressed(int key) {
    switch(key) {
        case 'g':
            toggleGUI();
            break;
        case 'e':
            toggleZDistance();
            break;
        case 'w':
            toggleRotation();
            break;
        case 'f':
            ofToggleFullscreen();
            break;
        case ' ':
            audioFile.toggle();
            break;
        case 's':
            wobbleHandler.makeScratch();
            break;
    }
}

//--------------------------------------------------------------
void Blob::mousePressed(int x, int y, int) {
    float xf = static_cast<float>(x);
    float yf = static_cast<float>(y);

    pointerStartPositionAbs.x = xf;
    pointerStartPositionAbs.y = yf;

    float xNorm = (xf - rect.x) / rect.width;
    float yNorm = (yf - rect.y) / rect.height;

    uniforms->mousePos.x = xNorm * 2.0f - 1.0f;
    uniforms->mousePos.y = (1.0f - yNorm) * 2.0f - 1.0f;

    maxMouseClickTime = ofGetElapsedTimeMillis() + 400;

    uniforms->mouseDownTarget = 1.0;

    wobbleHandler.onMouseDown();
}

//--------------------------------------------------------------
void Blob::mouseDragged(int x, int y, int) {
    auto xf = static_cast<float>(x);
    auto yf = static_cast<float>(y);

    auto xNorm = (xf - rect.x) / rect.width;
    auto yNorm = (yf - rect.y) / rect.height;

    uniforms->mousePos.x = xNorm * 2.0f - 1.0f;
    uniforms->mousePos.y = (1.0f - yNorm) * 2.0f - 1.0f;

    wobbleHandler.onDrag();
}

//--------------------------------------------------------------
void Blob::mouseReleased(int x, int y, int) {
    wobbleHandler.onMouseUp();
    mouseExited(x, y);
}

//--------------------------------------------------------------
void Blob::mouseExited(int, int) {
    uniforms->mouseDownTarget = 0.0;
    isDragging = false;
}


//--------------------------------------------------------------
void Blob::windowResized(int w, int h) {
    rect.setWidth(w);
    rect.setHeight(h);
    uniforms->aspectRatio = w * 1.0f / h;
    uniforms->screenParams.x = w;
    uniforms->screenParams.y = h;
    uniforms->screenParams.z = 1.0f / w;
    uniforms->screenParams.w = 1.0f / h;
    cam->setAspectRatio(uniforms->aspectRatio);
}

//--------------------------------------------------------------
void Blob::update() {
    Interpolator::updateAll();

    audioFile.update(gui->songSpectrumSmoothing);
    audioLive.update(gui->microphoneSpectrumSmoothing);

    ofSetBackgroundColor(gui->backgroundColor.get());

    auto delta = min(1.0 / 20.0, ofGetLastFrameTime());
    uniforms->time = fmod(uniforms->time + delta, 1000.0f);
    uniforms->yRotation += rotationSpeed * delta * gui->yRotationSpeed;

    {
        auto & bands = audioLive.bands;
        for(auto i = 0; i < bands.length(); ++i) {
            if(uniforms->volumeMicrophone[i] < bands[i] * gui->microphoneVolumeScaler) {
                uniforms->volumeMicrophone[i] = bands[i] * gui->microphoneVolumeScaler;
            } else {
                uniforms->volumeMicrophone[i] =
                    ofLerp(uniforms->volumeMicrophone[i], 0.0, delta * gui->microphoneFadeSpeed);
            }
        }
    }

    {
        auto & bands = audioFile.bands;
        for(auto i = 0; i < bands.length(); ++i) {
            if(uniforms->volumeSong[i] < bands[i] * gui->songVolumeScaler) {
                uniforms->volumeSong[i] = bands[i] * gui->songVolumeScaler;
            } else {
                uniforms->volumeSong[i] =
                    ofLerp(uniforms->volumeSong[i], 0.0, delta * gui->songFadeSpeed);
            }
        }
    }

    slowdownSpeed = ofLerp(
            slowdownSpeed,
            slowdownToggle ? gui->slowdownScale.get() : 1.0f,
            delta * 2.0);

    auto minMovement =
        audioLive.anyBandAbove(gui->movementMicrophoneMinValue) ||
        audioFile.anyBandAbove(gui->movementSongMinValue) ||
        speedUpToggle;
    auto movementSpeedTarget = (minMovement ?
            gui->movementAudioModifier_speedWithSound :
            gui->movementAudioModifier_speedWithoutSound) * 1.0;

    auto minShape =
        audioLive.anyBandAbove(gui->shapeMicrophoneMinValue) ||
        audioFile.anyBandAbove(gui->shapeSongMinValue) ||
        speedUpToggle;
    auto shapeSpeedTarget = (minShape ?
            gui->shapeAudioModifier_speedWithSound :
            gui->shapeAudioModifier_speedWithoutSound) * 1.0;

    // Clamp movement/shape speed
    {
        auto k = 0.8f;
        if(!speedUpToggle) {
            k = ofClamp(1.3f * max(
                        audioLive.bandMaxValue - gui->movementMicrophoneMinValue,
                        audioFile.bandMaxValue - gui->shapeMicrophoneMinValue
                    ), 0.0f, 1.0f);
        }
        movementSpeedTarget *= k;
        shapeSpeedTarget *= k;
    }

    {
        float t = movementTimeDeltaMultiplier < movementSpeedTarget ?
            gui->movementAudioModifier_fadeSpeedUp :
            gui->movementAudioModifier_fadeSpeedDown;

        movementTimeDeltaMultiplier =
            ofLerp(movementTimeDeltaMultiplier, movementSpeedTarget, delta * t);
    }

    {
        auto triggerVol = 1.0f - gui->wrinkle_soundSens;
        if(audioLive.anyBandAbove(triggerVol) ||
            audioFile.anyBandAbove(triggerVol)) {
            wobbleHandler.makeScratch();
        }
    }

    {
        float t = shapeTimeDeltaMultiplier < shapeSpeedTarget ?
            gui->shapeAudioModifier_fadeSpeedUp :
            gui->shapeAudioModifier_fadeSpeedDown;

        shapeTimeDeltaMultiplier =
            ofLerp(shapeTimeDeltaMultiplier, shapeSpeedTarget, delta * t);
    }

    {
        float ds = delta * slowdownSpeed * shapeTimeDeltaMultiplier;
        float dm = delta * slowdownSpeed * movementTimeDeltaMultiplier;
        uniforms->timeShapeNoise =
            fmod(uniforms->timeShapeNoise +
                gui->shapeNoiseData_speed * ds, 1000.0f);
        uniforms->timeBigNoise =
            fmod(uniforms->timeBigNoise +
                gui->bigNoiseData_speed * ds, 1000.0f);
        uniforms->timeSmallNoise =
            fmod(uniforms->timeSmallNoise +
                 gui->smallNoiseData_speed * ds, 1000.0f);
        uniforms->timePositionNoise =
            fmod(uniforms->timePositionNoise +
                gui->positionNoiseData_speed * dm, 1000.0f);
        uniforms->timeBehaviourNoise =
            fmod(uniforms->timeBehaviourNoise +
                gui->behaviourNoiseData_speed * ds, 1000.0f);
        uniforms->timeDeflateNoise =
            fmod(uniforms->timeDeflateNoise +
                gui->deflateNoiseData_speed * ds, 1000.0f);
        uniforms->timeAudioReactivenessNoise =
            fmod(uniforms->timeAudioReactivenessNoise +
                 gui->audioReactivenessNoise_speed * ds, 1000.0f);
        uniforms->timeNoiseNormal0 =
            fmod(uniforms->timeNoiseNormal0 +
                gui->noiseNormalSpeed0 * ds, 1000.0f);
        uniforms->timeNoiseNormal1 =
            fmod(uniforms->timeNoiseNormal1 +
                gui->noiseNormalSpeed1 * ds, 1000.0f);
    }

    wobbleHandler.update(delta);

    // runSlowdownToggle
    if(ofGetElapsedTimef() > speedUpTime) {
        speedUpToggle ^= true;
        speedUpTime +=
            (speedUpToggle ?
                ofRandom(gui->spedUpMinDuration, gui->spedUpMaxDuration) :
                ofRandom(gui->speedUpMinDuration, gui->speedUpMaxDuration));
    }

    // runSpeedUpToggle
    if(ofGetElapsedTimef() > slowdownTime) {
        slowdownToggle ^= true;
        slowdownTime +=
            (slowdownToggle ?
                ofRandom(gui->slowedDownMinDuration, gui->slowedDownMaxDuration) :
                ofRandom(gui->slowdownMinDuration, gui->slowdownMaxDuration));
    }
}

//--------------------------------------------------------------
void Blob::drawDebug() {
    // text with audioFile player position
    ofDrawBitmapString(audioFile.getPlayerPos(), 50.f, 50.f, 0.f);
    auto y = ofGetHeight() - 20.f;

    for(auto i = 0; i < audioFile.bands.length(); ++i) {
        ofDrawRectangle(50 + 10 * i, y, 5,
            -100 * audioFile.bands[i]);
    }

    for(auto i = 0; i < audioLive.bands.length(); ++i) {
        ofDrawRectangle(50.0f + 10 * i, y - 100, 5,
            -100 * audioLive.bands[i]);
    }

    for (size_t i = 0; i < audioFile.spectrum.size(); i++) {
        ofDrawRectangle(100 + 3 * i, y, 2,
            -audioFile.spectrum[i] * 100);
    }

    for (size_t i = 0; i < audioLive.spectrum.size(); i++) {
        ofDrawRectangle(100 + 3 * i, y - 100, 2,
            -audioLive.spectrum[i] * 100);
    }

    ofDrawRectangle(50, y + 5, audioFile.rms * 300, 5);
    ofDrawRectangle(50, y - 100 + 5, audioLive.rms * 300, 5);
}

//--------------------------------------------------------------
void Blob::draw() {
    ofFill();
    ofSetColor(ofColor::white);

    wobbleHandler.draw(gui->debug);

    if(gui->debug) {
        drawDebug();
    }
}

//--------------------------------------------------------------
void Blob::drawGui(ofEventArgs &) {
    if(guiVisible) {
        gui->draw();
    }
}

//--------------------------------------------------------------
void Blob::exit() {
    audioFile.exit();
    audioLive.exit();
    ofSoundStreamStop();
}
