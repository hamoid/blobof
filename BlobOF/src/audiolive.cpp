#include "audiolive.h"

//--------------------------------------------------------------
void AudioLive::setup(shared_ptr<BlobConfig> cfg) {
    this->cfg = cfg;
    settings.setInListener(this);
    settings.numInputChannels = cfg->audioInputNumChannels;
    settings.sampleRate = cfg->audioInputSampleRate;
    analyzer.setup(
        static_cast<int>(settings.sampleRate),
        static_cast<int>(settings.bufferSize),
        static_cast<int>(settings.numInputChannels)
    );
    auto devices = stream.getDeviceList();
    for(int i = 0; i < devices.size(); i++) {
        std::cout << "Audio device: " << i <<
            " (" << devices[i].name << ")" << std::endl <<
            "  Inputs: " << devices[i].inputChannels << std::endl <<
            "  Freqs: " << devices[i].sampleRates << std::endl <<
            "  Default: " << (devices[i].isDefaultInput ? "yes" : "no") << std::endl <<
            std::endl;
    }
    stream.setup(settings);
}

//--------------------------------------------------------------
void AudioLive::audioIn(ofSoundBuffer & inBuffer) {
    inBuffer *= volumeCurr;
    analyzer.analyze(inBuffer);
}

//--------------------------------------------------------------
bool AudioLive::anyBandAbove(float threshold) {
    return bandMaxValue > threshold;
}

//--------------------------------------------------------------
float AudioLive::average(vector<float> & data, size_t a, size_t b) {
    float sum { 0.0f };

    for(size_t i = a; i < b; ++i) {
        sum += data[i];
    }

    return sum / static_cast<float>(b - a);
}

//--------------------------------------------------------------
void AudioLive::update(float smooth) {
    rms = analyzer.getValue(RMS, 0, smooth);
    spectrum = analyzer.getValues(SPECTRUM, 0, smooth);

    for(auto & v : spectrum) {
        v = ofMap(v, DB_MIN, DB_MAX, 0.0, 1.0, true);
    }

    // spectrum.size() = 129
    // 4 values (sub, low, mid, high)
    bands[0] = average(spectrum, 0, 8);    // 8
    bands[1] = average(spectrum, 8, 24);   // 16
    bands[2] = average(spectrum, 24, 56);  // 32
    bands[3] = average(spectrum, 56, 116); // 60

    bandMaxValue = max(max(bands[0], bands[1]),
            max(bands[2], bands[3]));

    if(volumeCurr != volumeNext) {
        volumeCurr = volumeNext;
    }
}

//--------------------------------------------------------------
void AudioLive::exit() {
    // Produces crash on exit
    // https://github.com/leozimmerman/ofxAudioAnalyzer/issues/11
    //analyzer.exit();

    stream.stop();
}

//--------------------------------------------------------------
void AudioLive::setVolume(float volNorm) {
    volumeNext = volNorm;
}
