#pragma once

#include "gui.h"
#include "uniforms.h"
#include <ofFbo.h>
#include <ofMesh.h>
#include <ofCamera.h>
#include <ofxAutoReloadedShader.h>

class WobbleHandler {
    private:
        shared_ptr<Uniforms> uniforms;
        shared_ptr<Gui> gui;
        shared_ptr<ofCamera> cam;
        shared_ptr<BlobConfig> cfg;

        bool isDragging { false };
        glm::vec3 avoidPositionTarget;

        ofVboMesh mesh;

        ofxAutoReloadedShader shdrWobbleSphere,
                              shdr1Positions,
                              shdr2Normals,
                              shdr3NormalsSmooth,
                              shdr4NormalsNoise,
                              shdrBlurX,
                              shdrBlurY,
                              shdrRejuvenator;

        ofTexture texPositions,
                  texNormals,
                  texNormalsSmooth,
                  texNormalsNoise;

        ofPlanePrimitive area, areaNoise;

        ofShortPixels pixelsPositions,
                      pixelsNormals,
                      pixelsNormalsSmooth;
        ofPixels pixelsNormalsNoise;

        ofFbo fboPositions,
              fboNormals,
              fboNormalsSmooth,
              fboNormalsNoise,
              fboWrinkles,
              fboWrinklesA,
              fboWrinklesB;

        float wrinkleBaseColor { 0.5 };

        void makeBlobGeometry();
        void resetScratchs();
        void configureArea(ofPlanePrimitive & a, const glm::vec2 & size);
        void updateMouseTargets();
    public:
        void setup(shared_ptr<Uniforms> uniforms,
            shared_ptr<Gui> gui,
            shared_ptr<ofCamera> cam,
            shared_ptr<BlobConfig> cfg);
        void update(double delta);
        void updatePositions();
        void updateNormals();
        void updateNormalsSmooth();
        void updateNormalsNoise();
        void updateScratches();
        void makeScratch();
        void render();
        void draw(bool debug);
        void onMouseDown();
        void onDrag();
        void onMouseUp();
};

