#include "ofMain.h"
#include "blob.h"
#include "ofAppGLFWWindow.h"

int main() {

    essentia::warningLevelActive = false;

    shared_ptr<BlobConfig> cfg = make_shared<BlobConfig>();

    // save config
    ofFile file("blobConfig.json"); //, ofFile::WriteOnly
    //jsonout json(file);
    //json << *cfg;

    // load config
    if(file.exists()) {
        jsonin json(file);
        json >> *cfg;
    }

    ofGLFWWindowSettings winSettings;
    winSettings.setGLVersion(cfg->glVersion[0], cfg->glVersion[1]);
    winSettings.setSize(cfg->winSize.x, cfg->winSize.y);
    winSettings.setPosition(cfg->winPos);
    winSettings.resizable = false;
    shared_ptr<ofAppBaseWindow> mainWindow = ofCreateWindow(winSettings);
    mainWindow->setVerticalSync(true);

    shared_ptr<ofAppBaseWindow> guiWindow;

    if(cfg->multiWindow) {
        winSettings.setSize(cfg->uiSize.x, cfg->uiSize.y);
        winSettings.setPosition(cfg->uiPos);
        // Share main's OpenGL resources with gui // optional
        //winSettings.shareContextWith = mainWindow;
        guiWindow = ofCreateWindow(winSettings);
        guiWindow->setVerticalSync(false);
    }

    shared_ptr<Blob> mainApp(new Blob);
    mainApp->setupGui(cfg);

    if(cfg->multiWindow) {
        ofAddListener(guiWindow->events().draw, mainApp.get(), &Blob::drawGui);
    } else {
        ofAddListener(mainWindow->events().draw, mainApp.get(), &Blob::drawGui);
    }

    if(ofIsGLProgrammableRenderer()) {
        ofRunApp(mainWindow, mainApp);
        ofRunMainLoop();
    } else {
        std::cout << "GLES / GL2 not implemented" << std::endl;
    }

}
