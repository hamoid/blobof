#include "gui.h"

std::string jsonFilename(std::string name) {
    return "gui" + name + ".json";
}

//--------------------------------------------------------------
void Gui::setup() {
    ofxGuiSetFont(ofToDataPath("fonts/Oxygen regular.ttf"), 9, true, true, 96);
    ofxGuiSetTextPadding(4);
    ofxGuiSetDefaultWidth(220);
    ofxGuiSetDefaultHeight(20);

    ofxGuiGroup::elementSpacing = 1;
    ofxGuiGroup::groupSpacing = 1;
    ofxGuiGroup::childrenLeftIndent = 15;
    ofxGuiGroup::childrenRightIndent = 2;

    float xx = 20.f;
    float yy = 20.f;

    {
        auto name { "Core" };
        colCore.setup(name, jsonFilename(name), xx, yy);
        colCore.setHeaderBackgroundColor(ofColor::darkCyan);

        colCore.add(yRotationSpeed);
        colCore.add(audioReactivenessNoise);
        colCore.add(movementAudioModifier);
        colCore.add(slowdownDuration);
        colCore.add(slowedDownDuration);
        colCore.add(slowdownScale);
        colCore.add(speedUpDuration);
        colCore.add(spedUpDuration);
        colCore.add(shapeAudioModifier);
        colCore.add(positionZDistance);
        colCore.add(behaviourNoiseData);
        colCore.add(positionNoiseData);
        colCore.add(debug);
        colCore.add(opacity);
        colCore.add(aaPower);
        colCore.add(aaScale);

        colCore.loadFromFile(jsonFilename(name));
    }

    {
        xx += 240;
        auto name { "Color" };
        colColor.setup(name, jsonFilename(name), xx, yy);
        colColor.setHeaderBackgroundColor(ofColor::darkorange);

        colColor.add(backgroundColor);
        colColor.add(baseColor);
        colColor.add(colorRim);
        colColor.add(colorSpot);

        colColor.loadFromFile(jsonFilename(name));
    }

    // ----
    // Microphone
    {
        xx += 240;
        auto name { "Microphone" };
        colMicrophone.setup(name, jsonFilename(name), xx, yy);
        colMicrophone.setHeaderBackgroundColor(ofColor::darkOrchid);

        colMicrophone.add(movementMicrophoneMinValue);
        colMicrophone.add(shapeMicrophoneMinValue);
        colMicrophone.add(microphoneFadeSpeed);
        colMicrophone.add(microphoneVolumeScaler);
        colMicrophone.add(microphoneSpectrumSmoothing);
        colMicrophone.add(microphoneScaleSpectrums);
        colMicrophone.add(microphoneScaleStrength);
        colMicrophone.add(microphoneBigNoise);
        colMicrophone.add(microphoneSmallNoise);

        colMicrophone.loadFromFile(jsonFilename(name));
    }

    {
        xx += 240;
        auto name { "Song" };
        auto path { ofToDataPath("sounds") };
        colSong.setup(name, jsonFilename(name), xx, yy);
        colSong.setHeaderBackgroundColor(ofColor::darkRed);

        songDropdown.populateFromDirectory(path, { "mp3" });
        colSong.add(& songDropdown);
        colSong.add(movementSongMinValue);
        colSong.add(shapeSongMinValue);
        colSong.add(songFadeSpeed);
        colSong.add(songVolumeScaler);
        colSong.add(songSpectrumSmoothing);
        colSong.add(songScaleSpectrums);
        colSong.add(songScaleStrength);
        colSong.add(songBigNoise);
        colSong.add(songSmallNoise);

        colSong.loadFromFile(jsonFilename(name));
    }

    {
        xx += 240;
        auto name { "Shape" };
        colShape.setup(name, jsonFilename(name), xx, yy);
        colShape.setHeaderBackgroundColor(ofColor::darkSeaGreen);

        colShape.add(deflateNoiseData);
        colShape.add(deflateNoiseGradientData);
        colShape.add(shapeNoiseData);
        colShape.add(bigNoiseData);
        colShape.add(smallNoiseData);
        colShape.add(rejuvenator);

        colShape.loadFromFile(jsonFilename(name));
    }

    {
        auto name { "Normals" };
        colNormals.setup(name, jsonFilename(name), xx, yy + colShape.getHeight() + 20);
        colNormals.setHeaderBackgroundColor(ofColor::darkSlateBlue);

        colNormals.add(normalProcessingSmoother);
        colNormals.add(normalNoise0);
        colNormals.add(normalNoise1);
        colNormals.add(noiseNormalAmount);
        colNormals.add(normalNoiseDistance);

        colNormals.loadFromFile(jsonFilename(name));
    }
}

//--------------------------------------------------------------
void Gui::draw() {
    colCore.draw();
    colMicrophone.draw();
    colColor.draw();
    colShape.draw();
    colNormals.draw();
    // last so song dropdown is on top of everything
    // (until fixed in orxDropdown)
    colSong.draw();
}

/**
 * @brief Gui::changeSong
 * @param indexDelta, normally +1 or -1
 */
void Gui::changeSong(int indexDelta) {
    auto sel = songDropdown.selectedValue;
    auto opts = songDropdown.getValues();
    // We need to find the current index
    auto it = std::find(opts.begin(), opts.end(), sel);
    // If no current song is found choose the 1st one
    if(it == opts.end()) {
        it = opts.begin();
    }
    auto numOpts = songDropdown.getNumOptions();
    auto indexOld = it - opts.begin();
    auto indexNew = (indexOld + indexDelta + numOpts) % numOpts;
    songDropdown.setSelectedValueByIndex(indexNew, true);
}
