#pragma once

#include "glm/vec2.hpp"
#include "glm/vec3.hpp"
#include "glm/vec4.hpp"
#include "interpolator.h"
#include "config.h"

// NOTE: these could become ofParameter to be able to specify min and max values.
// NOTE: Find other usages of ofParameter in the project.
class Uniforms {
    public:
        float time { 0.0 };
        float timeShapeNoise             { 1.1 };
        float timeBigNoise               { 2.2 };
        float timeSmallNoise             { 3.3 };
        float timePositionNoise          { 4.4 };
        float timeBehaviourNoise         { 5.5 };
        float timeDeflateNoise           { 6.6 };
        float timeAudioReactivenessNoise { 7.7 };
        float timeNoiseNormal0           { 8.8 };
        float timeNoiseNormal1           { 9.9 };

        float aspectRatio { 1.0 };
        float mouseDownTarget { 0.0 };
        float yRotation { 0.0 };
        float mouseDownCurrent { 0.0f };

        glm::vec2 mousePos { 0.0, 1.0 };

        glm::vec3 cameraPosition { 0.004f };
        glm::vec3 cursorWorldTarget;
        glm::vec3 cursorWorldCurrent;
        glm::vec3 avoidPositionCurrent;
        glm::vec3 attractPositionCurrent;

        glm::vec4 volumeMicrophone { 0.0 };
        glm::vec4 volumeSong { 0.0 };
        glm::vec4 screenParams { 1.0, 1.0, 0.0, 0.0 };

        Interpolator positionZ { 0.0, 0.01, 0.05, 0.1 };
};
