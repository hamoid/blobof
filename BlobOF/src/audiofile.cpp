#include "audiofile.h"

//--------------------------------------------------------------
void AudioFile::setup(shared_ptr<BlobConfig> cfg) {
    this->cfg = cfg;
}

//--------------------------------------------------------------
bool AudioFile::anyBandAbove(float threshold) {
    return bandMaxValue > threshold;
}

//--------------------------------------------------------------
float AudioFile::average(vector<float> & data, size_t a, size_t b) {
    float sum { 0.0f };

    for(size_t i = a; i < b; ++i) {
        sum += data[i];
    }

    return sum / static_cast<float>(b - a);
}

//--------------------------------------------------------------
void AudioFile::update(float smooth) {
    if(!player.isPlaying()) {
        return;
    }
    buffer = player.getCurrentSoundBuffer(static_cast<int>(settings.bufferSize));
    try {
        // This crashes on Mac. Maybe it's when the sound loops for the first time,
        // not sure, but the try/catch avoids the crash.
        analyzer.analyze(buffer);
    } catch(essentia::EssentiaException & e) {
        std::cout << "ERR: " << e.what() << std::endl;
    }

    rms = analyzer.getValue(RMS, 0, smooth);
    spectrum = analyzer.getValues(SPECTRUM, 0, smooth);

    for(auto & v : spectrum) {
        v = ofMap(v, DB_MIN, DB_MAX, 0.0, 1.0, true);
    }

    // spectrum.size() = 129
    // 4 values (sub, low, mid, high)
    bands[0] = average(spectrum, 0, 8);    // 8
    bands[1] = average(spectrum, 8, 24);   // 16
    bands[2] = average(spectrum, 24, 56);  // 32
    bands[3] = average(spectrum, 56, 116); // 60

    bandMaxValue = max(max(bands[0], bands[1]),
            max(bands[2], bands[3]));

    if(volumeCurr != volumeNext) {
        volumeCurr = volumeNext;
        player.setVolume(volumeCurr);
    }

    // for(auto i = 0; i < buffer.getNumFrames(); i++) {
    //   auto sample = buffer.getSample(i, ch); // -1 .. +1
    // }
}

//--------------------------------------------------------------
void AudioFile::toggle() {
    player.setPaused(!player.isPaused());
}

//--------------------------------------------------------------
void AudioFile::play() {
    player.setPaused(false);
}

//--------------------------------------------------------------
void AudioFile::pause() {
    player.setPaused(true);
}

//--------------------------------------------------------------
void AudioFile::load(ofFile file) {
    bool wasPlaying = player.isPlaying();
    if(wasPlaying) {
        player.stop();
        player.unload();
    }
    player.load(file.path());
    player.setVolume(volumeCurr);
    player.setLoop(true);

    settings.numInputChannels = static_cast<size_t>(player.getNumChannels());
    settings.sampleRate = static_cast<size_t>(player.getSampleRate());

    if(isAnalyzerSetup) {
        analyzer.reset(
            static_cast<int>(settings.sampleRate),
            static_cast<int>(settings.bufferSize),
            static_cast<int>(settings.numInputChannels)
        );
    } else {
        analyzer.setup(
            static_cast<int>(settings.sampleRate),
            static_cast<int>(settings.bufferSize),
            static_cast<int>(settings.numInputChannels)
        );
        isAnalyzerSetup = true;
    }

    if(wasPlaying) {
        player.play();
    }
}

//--------------------------------------------------------------
void AudioFile::exit() {
    // Produces crash on exit on Mac
    // https://github.com/leozimmerman/ofxAudioAnalyzer/issues/11
    // Commenting it out does not help.
    analyzer.exit();

    player.close();
}

//--------------------------------------------------------------
void AudioFile::setVolume(float volNorm) {
    volumeNext = volNorm;
}

//--------------------------------------------------------------
float AudioFile::getPlayerPos() {
    return static_cast<float>(player.getPositionMS()) / 1000.f;
}
