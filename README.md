### Dependencies

- [ofxAutoReloadedShader](https://github.com/andreasmuller/ofxAutoReloadedShader/tree/77f7a2141c0e912c09ae5bf414210fdf612b6b46)
- [ofxAudioAnalyzer](https://github.com/hamoid/ofxAudioAnalyzer/tree/62e0cb65fa8201cee25fd2434857d8ae56673932)
- [ofxRaycaster](https://github.com/edap/ofxRaycaster/tree/7a0a686a77011ba7021e8d77d76223b2c4a939e4)
- ofxGui: built-in in version [1a67b05](https://github.com/openframeworks/openframeworks/tree/1a67b05908dde630b4df260c29b2096373a507e3)
- ofxOsc: built-in in version [1a67b05](https://github.com/openframeworks/openframeworks/tree/1a67b05908dde630b4df260c29b2096373a507e3)


### See

- https://threejs.org/docs/#api/en/core/Raycaster

## ofxAudioAnalyzer

This addon has 3 static libraries compiled years ago, before the Apple M1 cpu
existed: `essentia`, `fftw3f` and `libsndfile`. Compiling them again is not
easy, I spent about two days trying. If compiling the most recent versions
it is very likely that `ofxAudioAnalyzer` does not communicate properly with
those libraries because their APIs have probably changed in recent years.

After trying with this approach I found out in the forum that one can disable
`arm64` support in xcode, forcing it to use Rosetta. It may be a bit slower,
but it works. I will leave the exercise of compiling thoes 3 libraries and
adapting ofxAudioAnalyzer for someone who enjoys this challenge.



