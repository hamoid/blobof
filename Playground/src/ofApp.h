#pragma once

#include "interpolator.h"
#include "ofMain.h"

//#define nanoseconds 1000000L

/**
 * Attempt at irregular events. BUT using a thread is not ideal because
 * I would need to use a lock to avoid updating graphics while the value
 * changes. It's very unlikely to happen, but I don't want the risk.
 * Better to use a nextEventMs and compare the current time to that inside
 * the graphics thread.
 */
//class Scheduler: public ofThread {
//    public:
//        Scheduler() {
//            timer.setPeriodicEvent(1 * 1000 * nanoseconds); // this is 1 second in nanoseconds
//            startThread();
//        }

//    private:
//        ofTimer timer;
//        void threadedFunction() {
//            while(isThreadRunning()) {
//                timer.waitNext();
//                auto nu = static_cast<int>(ofRandom(1, 5));
//                std::cout << nu << std::endl;
//                timer.setPeriodicEvent(nu * 1000 * nanoseconds);
//            }
//        }
//};

class ofApp : public ofBaseApp {

    public:
        void setup();
        void update();
        void draw();

        void keyPressed(int key);
        void keyReleased(int key);
        void mouseMoved(int x, int y );
        void mouseDragged(int x, int y, int button);
        void mousePressed(int x, int y, int button);
        void mouseReleased(int x, int y, int button);
        void mouseEntered(int x, int y);
        void mouseExited(int x, int y);
        void windowResized(int w, int h);
        void dragEvent(ofDragInfo dragInfo);
        void gotMessage(ofMessage msg);

//        Scheduler sched;

        Interpolator x { 200.0, 0.5, 10.0, 200.0 };
        Interpolator y { 200.0, 0.5, 10.0, 200.0 };
        Interpolator sz { 0.0, 0.01, 0.05, 0.1 };
};
