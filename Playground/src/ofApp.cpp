#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup() {
    ofSetVerticalSync(true);
    ofBackground(34);
    ofSetFrameRate(60);
}

//--------------------------------------------------------------
void ofApp::update() {
    Interpolator::updateAll();
}

//--------------------------------------------------------------
void ofApp::draw() {
    ofSetColor(ofColor::antiqueWhite);
    ofDrawCircle(x, y, 10.0 + sz * 40.0);
}

//--------------------------------------------------------------
void ofApp::keyPressed(int) {
    sz >> (sz < 0.5 ? 1.0 : 0.0);
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key) {

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ) {

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button) {

}

//--------------------------------------------------------------
void ofApp::mousePressed(int _x, int _y, int button) {
}

//--------------------------------------------------------------
void ofApp::mouseReleased(int _x, int _y, int button) {
    x >> _x;
    y >> _y;
}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y) {

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y) {

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h) {

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg) {

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo) {

}
